-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 12 Sep 2020 pada 16.40
-- Versi Server: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kerjasama_stmik`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id_admin` int(11) NOT NULL,
  `nama_admin` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_admin`
--

INSERT INTO `tb_admin` (`id_admin`, `nama_admin`, `username`, `password`, `status`) VALUES
(1, 'Ketua STMIK Indonesia', 'ketua', '12345', 'Ketua'),
(2, 'WK3 BIDANG KERJASAMA', 'wk3', '12345', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_draf_mou`
--

CREATE TABLE `tb_draf_mou` (
  `id_draf` int(11) NOT NULL,
  `file_draf_mou_stmik` varchar(255) DEFAULT NULL,
  `id_proposal` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_draf_mou`
--

INSERT INTO `tb_draf_mou` (`id_draf`, `file_draf_mou_stmik`, `id_proposal`, `id_user`) VALUES
(1, 'f7eur-2020-09-11-Draft MoU.pdf', 1, NULL),
(2, '5iujr-2020-09-11-Draft MoU.pdf', 2, NULL),
(3, '50ma6-2020-09-12-Draft MoU.pdf', 3, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_file_mou`
--

CREATE TABLE `tb_file_mou` (
  `id_mou` int(11) NOT NULL,
  `file_mou_stmik` varchar(255) DEFAULT NULL,
  `tgl_mou` date DEFAULT NULL,
  `ttd_stmik` varchar(50) DEFAULT NULL,
  `ttd_mitra` varchar(50) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_proposal` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_file_mou`
--

INSERT INTO `tb_file_mou` (`id_mou`, `file_mou_stmik`, `tgl_mou`, `ttd_stmik`, `ttd_mitra`, `id_user`, `id_proposal`) VALUES
(1, 'STMIK11001-MoU-STMIK-I-I-2017-2020-09-11-001_SMK XIII KOTO KAMPAR,.pdf', '2017-01-09', 'Prof. Dr.-Ing. Ir. Hairul Abral', NULL, NULL, 1),
(2, 'STMIK11002-MoU-STMIK-I-I-2017-2020-09-11-002_FOROM MGMP MATEMATIKA SMA SUMBAR,,.pdf', '2017-01-29', 'Prof. Dr.-Ing. Ir. Hairul Abral', NULL, NULL, 2),
(3, 'STMIK11003-MoU-STMIK-I-I-2017-2020-09-12-003_SMA 1 KOTO XI TARUSAN,.pdf', '2017-01-30', 'Prof. Dr.-Ing. Ir. Hairul Abral', NULL, NULL, 3),
(4, 'STMIK11052-MoU-STMIK-I-I-2017-2020-09-12-BANK NAGARI.pdf', '2017-04-30', 'Prof. Dr, -Ing. Ir. H. Hairul Abral', NULL, NULL, 4),
(5, 'STMIK11031-MoU-STMIK-I-I-2017-2020-09-12-UNIVERSITAS GUNADARMA.pdf', '2017-02-23', 'Prof. Dr.-Ing. Ir. Hairul Abral', NULL, NULL, 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_hasil_kerjasama`
--

CREATE TABLE `tb_hasil_kerjasama` (
  `id_hasil_kerjasama` int(11) NOT NULL,
  `id_proposal` int(11) DEFAULT NULL,
  `hasil_kerjasama` text,
  `kerjasama_selanjutnya` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_hasil_kerjasama`
--

INSERT INTO `tb_hasil_kerjasama` (`id_hasil_kerjasama`, `id_proposal`, `hasil_kerjasama`, `kerjasama_selanjutnya`) VALUES
(1, 1, 'kerjasama yang saling menguntungkan antara kedua belah pihak<br>', '<p>-<br></p>'),
(2, 2, '<p>Kerjasama yang saling menguntungkan antara kedua belah pihak.<br></p>', '<p>-<br></p>'),
(3, 3, '<p>Kerjasama yang saling menguntungkan antara kedua belah pihak. <br></p>', '<p>-<br></p>'),
(4, 4, '<p>Kerjasama yang saling menguntungkan antara kedua belah pihak.&nbsp;<br></p>', '<p>-</p>'),
(5, 5, '<p>Kerjasama yang saling menguntungkan antara kedua belah pihak. <br></p>', '<p>-<br></p>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_jawaban`
--

CREATE TABLE `tb_jawaban` (
  `id_jawaban` int(11) NOT NULL,
  `id_pertanyaan` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_proposal` int(11) DEFAULT NULL,
  `jawaban` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_jawaban`
--

INSERT INTO `tb_jawaban` (`id_jawaban`, `id_pertanyaan`, `id_user`, `id_proposal`, `jawaban`) VALUES
(1, 2, 1, 1, '5'),
(2, 1, 1, 1, '2'),
(3, 12, 1, 1, '4'),
(4, 4, 1, 1, '2'),
(5, 10, 1, 1, '3'),
(6, 7, 1, 1, '5'),
(7, 11, 1, 1, '3'),
(8, 6, 1, 1, '4'),
(9, 3, 1, 1, '4'),
(10, 9, 1, 1, '2'),
(11, 5, 1, 1, '5'),
(12, 8, 1, 1, '4'),
(13, 1, 3, 3, '5'),
(14, 2, 3, 3, '2'),
(15, 7, 3, 3, '2'),
(16, 6, 3, 3, '3'),
(17, 5, 3, 3, '4'),
(18, 10, 3, 3, '4'),
(19, 9, 3, 3, '2'),
(20, 3, 3, 3, '2'),
(21, 4, 3, 3, '3'),
(22, 12, 3, 3, '4'),
(23, 11, 3, 3, '5'),
(24, 8, 3, 3, '4'),
(25, 11, 4, 4, '5'),
(26, 1, 4, 4, '3'),
(27, 7, 4, 4, '4'),
(28, 4, 4, 4, '2'),
(29, 2, 4, 4, '4'),
(30, 3, 4, 4, '3'),
(31, 12, 4, 4, '5'),
(32, 8, 4, 4, '4'),
(33, 5, 4, 4, '2'),
(34, 9, 4, 4, '4'),
(35, 8, 5, 5, '3'),
(36, 3, 5, 5, '4'),
(37, 2, 5, 5, '3'),
(38, 11, 5, 5, '4'),
(39, 10, 5, 5, '4'),
(40, 5, 5, 5, '4'),
(41, 9, 5, 5, '2'),
(42, 1, 5, 5, '2'),
(43, 6, 5, 5, '4'),
(44, 4, 5, 5, '4'),
(45, 7, 5, 5, '3'),
(46, 12, 5, 5, '4');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_jawaban_saran`
--

CREATE TABLE `tb_jawaban_saran` (
  `id_jawaban_saran` int(11) NOT NULL,
  `id_pertanyaan_saran` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_proposal` int(11) DEFAULT NULL,
  `jawaban_saran` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_jawaban_saran`
--

INSERT INTO `tb_jawaban_saran` (`id_jawaban_saran`, `id_pertanyaan_saran`, `id_user`, `id_proposal`, `jawaban_saran`) VALUES
(1, 1, 1, 1, 'tetap komunikasi'),
(2, 2, 1, 1, 'sangat bagus'),
(3, 2, 3, 3, 'bagus'),
(4, 1, 3, 3, 'tetap komunikasi'),
(5, 1, 5, 5, 'tetap komunikasi'),
(6, 2, 5, 5, '-');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kerjasama_luar_negri`
--

CREATE TABLE `tb_kerjasama_luar_negri` (
  `id_proposal_luar_negri` int(11) NOT NULL,
  `lembaga_mitra` varchar(50) DEFAULT NULL,
  `bidang_kerja_sama` varchar(50) DEFAULT NULL,
  `masa_berlaku` year(4) DEFAULT NULL,
  `hasil_kerjasama` text,
  `file_mou` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kerjasama_luar_negri`
--

INSERT INTO `tb_kerjasama_luar_negri` (`id_proposal_luar_negri`, `lembaga_mitra`, `bidang_kerja_sama`, `masa_berlaku`, `hasil_kerjasama`, `file_mou`) VALUES
(1, 'DAYEH UNIVERISTY TAIWAN (COOPERATE KOPERTISWILAYAH', 'Tri Darma Perguruan Tinggi', 2021, '<p>Kerjasama yang saling menguntungkan antara kedua belah pihak. <br></p>', 'y3v5u-DAYEH UNIVERISTY TAIWAN (COOPERATE KOPERTISWILAYAH X).pdf'),
(2, 'ming chi university of technology taiwan', 'Tri Darma Perguruan Tinggi', 2021, '<p>Kerjasama yang saling menguntungkan antara kedua belah pihak. <br></p>', 'j5b2a-ming chi university of technology taiwan.pdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pertanyaan_kusioner`
--

CREATE TABLE `tb_pertanyaan_kusioner` (
  `id_pertanyaan` int(11) NOT NULL,
  `kategori_pertanyaan` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_pertanyaan_kusioner`
--

INSERT INTO `tb_pertanyaan_kusioner` (`id_pertanyaan`, `kategori_pertanyaan`) VALUES
(1, 'Proses pembuatan naskah kerjasama (MoU) dilakukan dengan\r\ncepat dan tepat'),
(2, 'Staf kerjasama bekerja professional dan responsif memenuhi\r\nkabutuhan mitra'),
(3, 'Staf kerjasama memberikan pelayanan dengan ramah '),
(4, 'Prosedur kerjasama selama menjalin kerjasama diberikan dengan mudah'),
(5, 'Stmik Indonesia memberikan pendampingan yang terbaik unutk\r\nmemenuhi kebutuhan mitra kerjasama'),
(6, 'Kerjasama atau kegiatan yang telah terealisasi sesuai dengan\r\nharapan mitra kerjasama'),
(7, 'Kami (mitra kerjasama) mendapatkan hal yang bermanfaat dari\r\nkerjasama antara institusi kami dan STMIK Indonesia\r'),
(8, 'Implementasi kerjasama sesuai dengan MoU'),
(9, 'STMIK Indonesia secara proaktif menghubungi kami (mitra\r\nkerjasama) untuk perencanaan implementasi sebagai tindak lanjut\r\nMoU yang telah ditandatangani'),
(10, 'Laporan akhir kerjasama dikomunikasikan dengan baik dan tepat\r\nwaktu'),
(11, 'Mekanisme pelaporan keuangan pada kegiatan kerjasama dengan\r\nSTMIK Indonesia tidak menyulitkan'),
(12, 'Kami (mitra kerjasama) akan kembali ke STMIK Indonesia dimasa\r\nmendatang untuk menjalin kerjasama dalam kegiatan lain');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pertanyaan_saran`
--

CREATE TABLE `tb_pertanyaan_saran` (
  `id_pertanyaan_saran` int(11) NOT NULL,
  `pertanyaan_saran` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_pertanyaan_saran`
--

INSERT INTO `tb_pertanyaan_saran` (`id_pertanyaan_saran`, `pertanyaan_saran`) VALUES
(1, 'Berdasarkan jaringan kerjasama\r\nmohon bapak ibu berkenan\r\nmenyebutkan hal-hal yang\r\ndibutuhkan institusi bapak/ibu\r\ndalam kaitannya dengan\r\nkeberlanjutan kerjasama dimasa\r\nakan datang\r'),
(2, 'Mohon tuliskan pendapat bapak/ibu\r\nsecara keseluruhan tentang\r\npelayanan STMIK Indonesia dalam\r\nkegiatan kerjasama');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_proposal_mitra`
--

CREATE TABLE `tb_proposal_mitra` (
  `id_proposal` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `nama_mitra` varchar(100) DEFAULT NULL,
  `jabatan` varchar(50) DEFAULT NULL,
  `no_telfon` double(100,0) DEFAULT NULL,
  `nama_instansi` varchar(100) DEFAULT NULL,
  `alamat_instansi` text,
  `tgl_pengajuan` date DEFAULT NULL,
  `bidang_kerjasan` text,
  `file_proposal` text,
  `surat_pengantar` varchar(100) NOT NULL,
  `status_proposal` varchar(100) DEFAULT NULL,
  `kategori_proposal` varchar(50) DEFAULT NULL,
  `no_mou` varchar(50) DEFAULT NULL,
  `no_mou_mitra` varchar(50) DEFAULT NULL,
  `status_qusioner` varchar(50) DEFAULT NULL,
  `masa_berlaku` year(4) DEFAULT NULL,
  `laporan_kerja_sama` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_proposal_mitra`
--

INSERT INTO `tb_proposal_mitra` (`id_proposal`, `id_user`, `nama_mitra`, `jabatan`, `no_telfon`, `nama_instansi`, `alamat_instansi`, `tgl_pengajuan`, `bidang_kerjasan`, `file_proposal`, `surat_pengantar`, `status_proposal`, `kategori_proposal`, `no_mou`, `no_mou_mitra`, `status_qusioner`, `masa_berlaku`, `laporan_kerja_sama`) VALUES
(1, 1, 'Mairil, S.Pd', 'Kepala Sekolah', 0, 'SMK N 1 XIII Koto Kampar', 'Jl. Raya Candi Muara Takus Desa Koto Tuo Kec. XIII Koto Kampar', '2017-01-09', 'bidang penelitian dan pengembangan ilmu', 'proposal_kosong.pdf', 'u5b39-2020-09-11-SURAT PENGANTAR.pdf', 'Di Setujui', 'Dalam Negeri', '11001/MoU/STMIK-I/I/2017', '421.3/SMKN 1 XIII KK/181', 'Selesai', 2020, ''),
(2, 2, 'Dra. Giatari Sarmalena, M.Si', 'Ketua', 751, 'Forom MGMP MATEMATIKA SMA PROVINSI SUMBAR', ' Jl. Situjuh Padang', '2017-01-29', 'bidang pendidikan dan pengajaran', 'proposal_kosong.pdf', '3bo8u-2020-09-11-SURAT PENGANTAR.pdf', 'Di Setujui', 'Dalam Negeri', '11002/MoU/STMIK-I/I/2017', '004/FMGMP MAT-SMA/SUMBAR/I/2017', 'Terkirim', 2022, ''),
(3, 3, 'Drs. Masdal Fitri, M.Si', 'Kepala Sekolah', 0, 'SMA N 1 KOTO I TARUSAN', 'Jl. Sabai Nan Aluih, Kab. Pesisir Selatan', '2017-01-30', 'bidang pengabdian kepada masyarakat', 'proposal_kosong.pdf', '8wc6j-2020-09-12-SURAT PENGANTAR.pdf', 'Di Setujui', 'Dalam Negeri', '11003/MoU/STMIK-I/I/2017', '047/I08.420.11/SMA.01/HM/2017', 'Terkirim', 2020, ''),
(4, 4, 'Manar Fuadi, SE, MM, QIA', 'Pimpinan', 0, 'Bank Nagari', 'Jln. Pemuda No.21, Padang', '2017-04-03', 'bidang kegiatan lainnya', 'proposal_kosong.pdf', 'mdo8v-2020-09-12-SURAT PENGANTAR.pdf', 'Di Setujui', 'Dalam Negeri', '11052/MoU/STMIK-I/I/2017', 'PKS/355/CU/UM/04-2017', 'Terkirim', 2022, ''),
(5, 5, 'Prof. Dr. E.S. Margianti, SE., MM ', 'Ketua', 0, 'UNIVERSITAS GUNADARMA', 'Jalan Margonda Raya 100 Depok', '2017-02-23', 'bidang pendidikan dan pengajaran', 'proposal_kosong.pdf', 'kt80v-2020-09-12-SURAT PENGANTAR.pdf', 'Di Setujui', 'Dalam Negeri', '11031/MoU/STMIK-I/I/2017', '060/REK/UG/II/2017', 'Selesai', 2020, ''),
(6, 6, 'Andarli, M.Pd', 'Kepala Sekolah', 0, 'SMAN 2 KOTO XI TARUSAN', 'Jalan Rimbo Gajah, No 3, Barung barung Belanatai, Kab. Pesisir Selatan', '2017-01-30', 'bidang pengabdian kepada masyarakat', 'proposal_kosong.pdf', 'cf64u-2020-09-12-SURAT PENGANTAR.pdf', 'Di Setujui', 'Dalam Negeri', '-', '-', '-', NULL, ''),
(7, 7, 'Drs. Ishasijon', 'Kepala Sekolah', 0, 'SMK N 2 PAINAN', 'Jl. Jendral Sudirman, Nagari. Sago, Kec. IV Jurai, Kab. Pesisir Selatan', '2017-01-30', 'bidang pengabdian kepada masyarakat', 'proposal_kosong.pdf', 'ozndy-2020-09-12-005_SMK 2 PAINAN,.pdf', 'Terkirim', 'Dalam Negeri', '-', '-', '-', NULL, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `nama_user` varchar(50) DEFAULT NULL,
  `email_user` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `nama_user`, `email_user`, `username`, `password`, `status`) VALUES
(1, 'SMK N 1 XIII Koto Kampar', 'smkn1xiiikotokampar@gmail.com', 'smkn1xiiikotokampar', '12345', 'user'),
(2, 'Forom MGMP MATEMATIKA SMA PROVINSI SUMBAR', 'Forom MGMP MATEMATIKA SMA PROVINSI SUMBAR@gmail.co', 'forommgmp', '12345', 'user'),
(3, 'SMA N 1 KOTO I TARUSAN', 'sman1kotoxitarusan@gmail', 'sman1kotoxitarusan', '12345', 'user'),
(4, 'Bank Nagari', 'banknagari@gmail.com', 'banknagari', '12345', 'user'),
(5, 'UNIVERSITAS GUNADARMA', 'gunadarma@gmail.com', 'gunadarma', '12345', 'user'),
(6, 'SMAN 2 KOTO XI TARUSAN', 'sman2kotoxitarusan@gmail.com', 'sman2kotoxitarusan', '12345', 'user'),
(7, 'SMK N 2 PAINAN', 'smkn2painan@gmail.com', 'smkn2painan', '12345', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `tb_draf_mou`
--
ALTER TABLE `tb_draf_mou`
  ADD PRIMARY KEY (`id_draf`);

--
-- Indexes for table `tb_file_mou`
--
ALTER TABLE `tb_file_mou`
  ADD PRIMARY KEY (`id_mou`);

--
-- Indexes for table `tb_hasil_kerjasama`
--
ALTER TABLE `tb_hasil_kerjasama`
  ADD PRIMARY KEY (`id_hasil_kerjasama`);

--
-- Indexes for table `tb_jawaban`
--
ALTER TABLE `tb_jawaban`
  ADD PRIMARY KEY (`id_jawaban`);

--
-- Indexes for table `tb_jawaban_saran`
--
ALTER TABLE `tb_jawaban_saran`
  ADD PRIMARY KEY (`id_jawaban_saran`);

--
-- Indexes for table `tb_kerjasama_luar_negri`
--
ALTER TABLE `tb_kerjasama_luar_negri`
  ADD PRIMARY KEY (`id_proposal_luar_negri`);

--
-- Indexes for table `tb_pertanyaan_kusioner`
--
ALTER TABLE `tb_pertanyaan_kusioner`
  ADD PRIMARY KEY (`id_pertanyaan`);

--
-- Indexes for table `tb_pertanyaan_saran`
--
ALTER TABLE `tb_pertanyaan_saran`
  ADD PRIMARY KEY (`id_pertanyaan_saran`);

--
-- Indexes for table `tb_proposal_mitra`
--
ALTER TABLE `tb_proposal_mitra`
  ADD PRIMARY KEY (`id_proposal`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_draf_mou`
--
ALTER TABLE `tb_draf_mou`
  MODIFY `id_draf` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_file_mou`
--
ALTER TABLE `tb_file_mou`
  MODIFY `id_mou` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_hasil_kerjasama`
--
ALTER TABLE `tb_hasil_kerjasama`
  MODIFY `id_hasil_kerjasama` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_jawaban`
--
ALTER TABLE `tb_jawaban`
  MODIFY `id_jawaban` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `tb_jawaban_saran`
--
ALTER TABLE `tb_jawaban_saran`
  MODIFY `id_jawaban_saran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_kerjasama_luar_negri`
--
ALTER TABLE `tb_kerjasama_luar_negri`
  MODIFY `id_proposal_luar_negri` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_pertanyaan_kusioner`
--
ALTER TABLE `tb_pertanyaan_kusioner`
  MODIFY `id_pertanyaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tb_pertanyaan_saran`
--
ALTER TABLE `tb_pertanyaan_saran`
  MODIFY `id_pertanyaan_saran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_proposal_mitra`
--
ALTER TABLE `tb_proposal_mitra`
  MODIFY `id_proposal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
