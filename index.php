<!DOCTYPE html>
<html class="no-js" lang="zxx">
    <head>
		<!-- Meta Tags -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="keywords" content="Site keywords here">
		<meta name="description" content="">
		<meta name='copyright' content=''>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<!-- Title -->
		<title>STMIK &minus; INDONESIA PADANG</title>
		
		<!-- Favicon -->
		<link rel="icon" type="image/jpeg" href="desain_index/images/logo.jpeg">
		
		<!-- Web Font -->
		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="desain_index/css/bootstrap.min.css">
		<!-- Font Awesome CSS -->
        <link rel="stylesheet" href="desain_index/css/font-awesome.min.css">
		<!-- Nice Select CSS -->
        <link rel="stylesheet" href="desain_index/css/niceselect.css">
		<!-- Fancy Box CSS -->
        <link rel="stylesheet" href="desain_index/css/jquery.fancybox.min.css">
		<!-- Fancy Box CSS -->
        <link rel="stylesheet" href="desain_index/css/cube-portfolio.min.css">
		<!-- Owl Carousel CSS -->
        <link rel="stylesheet" href="desain_index/css/owl.carousel.min.css">
		<!-- Animate CSS -->
        <link rel="stylesheet" href="desain_index/css/animate.min.css">
		<!-- Slick Nav CSS -->
        <link rel="stylesheet" href="desain_index/css/slicknav.min.css">
        <!-- Magnific Popup -->
		<link rel="stylesheet" href="desain_index/css/magnific-popup.css">
		
		<!-- Eduland Stylesheet -->
        <link rel="stylesheet" href="desain_index/css/normalize.css">
        <link rel="stylesheet" href="desain_index/style.css">
        <link rel="stylesheet" href="desain_index/css/responsive.css">
		
		<!-- Eduland Colors -->
		<link rel="stylesheet" href="desain_index/css/colors/color1.css">
		
    </head>
    <body>
	
		<!-- Header -->
		<header class="header">
			<!-- Header Inner -->
			<div class="header-inner overlay">
				<div class="container">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-12">
							<!-- Logo -->
							<div class="logo">
								<a href="index.php"><img src="portal/gambar/logo.jpeg" alt="#"></a>
							</div>
							<!--/ End Logo -->
							<div class="mobile-menu"></div>
						</div>
						<div class="col-lg-9 col-md-9 col-12">
							<div class="menu-bar">
								<nav class="navbar navbar-default">
									<div class="navbar-collapse">
										<!-- Main Menu -->
										<ul id="nav" class="nav menu navbar-nav">
											<li class="active"><a href="index.php"><i class="fa fa-home"></i>Home</a></li>
											<!-- <li><a href="#"><i class="fa fa-clone"></i>Pages</a> 
												<ul class="dropdown">
													<li><a href="teachers.html">Teachers</a></li>
												</ul>
											</li> -->
											<li><a href="https://stmikindonesia.ac.id/"><i class="fa fa-clone"></i>KE WEBSITE STMIK INDONESIA</a></li>
											<!-- <li><a href="courses.html"><i class="fa fa-clone"></i>Download petunjuk</a></li> -->
											<li><a href="portal/"><i class="fa fa-bullhorn"></i>Login</a></li>
											<li><a href="portal/register.php"><i class="fa fa-address-book"></i>Daftar</a> </li>
										</ul>
										<!-- End Main Menu -->
									</div> 
								</nav>
								<!-- Search Area -->
								<div class="search-area">
									<a href="#header" class="icon"><i class="fa fa-search"></i></a>
									<form class="search-form">
										<input type="text" placeholder="ex: premium course" name="search">
										<button value="search " type="submit"><i class="fa fa-search"></i></button>
									</form>
								</div>	
								<!-- End Search Area-->
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--/ End Header Inner -->
		</header>
		<!--/ End Header -->
		
		<!-- Slider Area -->
		<section class="home-slider">
			<div class="slider-active">
				<!-- Single Slider -->
				<div class="single-slider overlay">
					<div class="slider-image" style="background-image:url('desain_index/images/slider/kerjasama.jpg')"></div>
					<div class="container">
						<div class="row">
							<div class="col-lg-7 col-md-10 col-12">
								<!-- Slider Content -->
								<div class="slider-content" >
									<h1 class="slider-title"><span>STMIK INDONESIA PADANG</span>Portal-<b>Kerjasama</b></h1>
									<p class="slider-text">Bergabunglah menjadi mitra STMIK Indonesia Padang ! Bersama membangun pendidikan yang lebih baik untuk Indonesia</p>
									<!-- Button -->
									<div class="button">
										<a href="portal/register.php" class="btn white primary">Daftar Kerjasama</a>
									</div>
									<!--/ End Button -->
								</div>
								<!--/ End Slider Content -->
							</div>
						</div>
					</div>
				</div>
				<!--/ End Single Slider -->
			</div>
		</section>
		<!--/ End Slider Area -->
		
		
		
		
	<!-- Faqs -->
		<section class="faqs section">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 offset-lg-3 col-12">
						<div class="section-title bg">
							<h2>BIDANG <span>KERJASAMA </span></h2>
							<p>STMIK INDONESIA PADANG</p>
							<div class="icon"><i class="fa fa-question"></i></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-5 col-12">
						<div class="faq-image">
							<img src="desain_index/images/faq.png" alt="#">
						</div>
					</div>
					<div class="col-lg-7 col-12">
						<div class="faq-main">
							<div class="faq-content">
								<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
									<!-- Single Faq -->
									<div class="panel panel-default">
										<div class="faq-heading"  id="FaqTitle1">
											<h4 class="faq-title">
												<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#faq1"><i class="fa fa-question"></i>Bidang pendidikan dan pengajaran</a>
											</h4>
										</div>
										<div id="faq1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="FaqTitle1">
											<div class="faq-body">
												<p>a. Peningkatan mutu sumberdaya manusia bidang pendidikan dan pengajaran. </p>
												<p>b. Penyelenggaraan pendidikan dan pengajaran. </p>
												<p>c. Jasa konsultasi mengenai pendidikan dan pengajaran.</p>
												<p>d. Studi banding dosen, mahasiswa dan staff</p>
												<p>e.  Kerjasama  pemerintah  daerah  dengan STMIK  Indonesiadalam  pelaksanaan Multi Kampus dengan Program Studi minimal berkualifikasi Akreditasi “B”</p>
												<p>f. kerjasama lainya</p>
											</div>
										</div>
									</div>
									<!--/ End Single Faq -->
									<!-- Single Faq -->
									<div class="panel panel-default active">
										<div class="faq-heading"  id="FaqTitle2">
											<h4 class="faq-title">
												<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#faq2"><i class="fa fa-question"></i>Bidang penelitian dan pengembangan ilmu</a>
											</h4>
										</div>
										<div id="faq2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="FaqTitle1">
											<div class="faq-body">
												<p>a.  penyelenggaraan  berbagai  kegiatan  penelitian  dan  konsultasi  mengenai penelitian.</p>
												<p>b. Peningkatan mutu sumberdaya manusia bidang penelitian.</p>
												<p>c.   Pendokumentasian,   penyebaran,   pemanfaatan   dan   pengembangan   hasil penelitian.</p>
												<p>d. kerjasama lainya</p>
											</div>
										</div>
									</div>
									<!--/ End Single Faq -->
									<!-- Single Faq -->
									<div class="panel panel-default">
										<div class="faq-heading"  id="FaqTitle3">
											<h4 class="faq-title">
												<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#faq3"><i class="fa fa-question"></i>Bidang pengabdian kepada masyarakat</a>
											</h4>
										</div>
										<div id="faq3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="FaqTitle3">
											<div class="faq-body">
												<p>a.Penyelenggaraan berbagai kegiatan pengabdian kepada masyarakat.</p>
												<p>b.Peningkatan mutu SDM dan sarana dan prasarana Pendidikan.</p>
												<p>c.Penyelenggaraan pendampingan dalam pengembangan perangkap pembelajaran bagi guru-guru SD, SMP/Sanawiah, SMA/MA dan SMK</p>
												<p>d.Pendokumentasian  dan  pemanfaatan  hasil  kegiatan  pengabdian  kepada masyarakat.</p>
												<p>e.Kerjasama Pemda dengan STMIK  Indonesiadalamkegiatan Kuliah Kerja Lapangan.</p>
												<p>f. kerjasama lainya</p>
											</div>
										</div>
									</div>
									<!--/ End Single Faq -->
									<!-- Single Faq -->
									<div class="panel panel-default">
										<div class="faq-heading"  id="FaqTitle4">
											<h4 class="faq-title">
												<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#faq4"><i class="fa fa-question"></i>Bidang kegiatan lainya </a>
											</h4>
										</div>
										<div id="faq4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="FaqTitle4">
											<div class="faq-body">
												<p>a.Peningkatan mutu manajemen pendidikan.</p>
												<p>b.Penerbitan bersama karya ilmiah melalui jurnal IJCS STMIK Indonesia.</p>
												<p>c.Penyelenggaraan seminar, lokakarya dan kegiatan ilmiah lainnya.</p>
												<p>d.Penggunaan perpustakaan.</p>
												<p>e.Penggunaan laboratoriumkomputerPengembangan dan Pusat Sumber Belajar STMIK Indonesia.</p>
												<p>f.Lain-lain fasilitas yang relevan dan dimiliki kedua belah pihak.</p>
											</div>
										</div>
									</div>
									<!--/ End Single Faq -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/ End Blogs -->
		
		<!-- Clients CSS -->
		<!--/ End Clients CSS -->
		
		<!-- Footer -->
		<footer class="footer section">
			<!-- Footer Top -->
			<div class="footer-top overlay">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 col-md-6 col-12">
							<!-- About -->
							<div class="single-widget about">
								<h2>ALAMAT</h2>
								<ul class="list">
									<li><i class="fa fa-phone"></i>Phone: +62-751-7056199 </li>
									<li><i class="fa fa-envelope"></i>Email: <a href="mailto:info@youremail.com"> sekretariat@stmikindonesia.ac.id </a></li>
									<li><i class="fa fa-map-o"></i>Jl. Khatib Sulaiman No.7a, Flamboyan Baru, Kec. Padang Bar., Kota Padang, Sumatera Barat</li>
								</ul>
								<!-- Social -->
								<ul class="social">
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li class="active"><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
									<li><a href="#"><i class="fa fa-youtube"></i></a></li>
								</ul>
								<!-- End Social -->
							</div>
							<!--/ End About -->
						</div>
						<div class="col-lg-4 col-md-6 col-12">
							<!-- Useful Links -->
							<div class="single-widget list">
								<h2>Link terkait</h2>
								<ul>
									<li><i class="fa fa-angle-right"></i><a href="#">WEBSITE STMIK INDONESIA PADANG</a></li>
									<li><i class="fa fa-angle-right"></i><a href="#">PORTAL KERJASAMA STMIK INDONESIA PADANG</a></li>
									<li><i class="fa fa-angle-right"></i><a href="#">.....</a></li>
								</ul>
							</div>
							<!--/ End Useful Links -->
						</div>
					</div>
				</div>
			</div>
			<!--/ End Footer Top -->
			<!-- Footer Bottom -->
			<div class="footer-bottom">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<!-- Copyright -->
							<div class="copyright">
								<p>© Copyright 2020. NovriaNurAini/161100191/TugasAkhir<br>Themplate By <a href="http://themelamp.com">www.themelamp.com</a>, Theme Release By <a href="http://codeglim.com">www.codeglim.com</a></p>
							</div>
							<!--/ End Copyright -->
						</div>
					</div>
				</div>
			</div>
			<!--/ End Footer Bottom -->
		</footer>
		<!--/ End Footer -->
		
		<!-- Jquery JS-->
        <script src="desain_index/js/jquery.min.js"></script>
        <script src="desain_index/js/jquery-migrate.min.js"></script>
		<!-- Colors JS-->
        <script src="desain_index/js/colors.js"></script>
		<!-- Popper JS-->
        <script src="desain_index/js/popper.min.js"></script>
		<!-- Bootstrap JS-->
        <script src="desain_index/js/bootstrap.min.js"></script>
		<!-- Owl Carousel JS-->
        <script src="desain_index/js/owl.carousel.min.js"></script>
		<!-- Jquery Steller JS -->
		<script src="desain_index/js/jquery.stellar.min.js"></script>
		<!-- Final Countdown JS -->
		<script src="desain_index/js/finalcountdown.min.js"></script>
		<!-- Fancy Box JS-->
		<script src="desain_index/js/facnybox.min.js"></script>
		<!-- Magnific Popup JS-->
		<script src="desain_index/js/jquery.magnific-popup.min.js"></script>
		<!-- Circle Progress JS -->
		<script src="desain_index/js/circle-progress.min.js"></script>
		<!-- Nice Select JS -->
		<script src="desain_index/js/niceselect.js"></script>
		<!-- Jquery Steller JS-->
        <script src="desain_index/js/jquery.stellar.min.js"></script>
		<!-- Jquery Steller JS-->
        <script src="desain_index/js/cube-portfolio.min.js"></script>
		<!-- Slick Nav JS-->
        <script src="desain_index/js/slicknav.min.js"></script>
		<!-- Easing JS-->
        <script src="desain_index/js/easing.min.js"></script>
		<!-- Waypoints JS-->
        <script src="desain_index/js/waypoints.min.js"></script>
		<!-- Counter Up JS -->
		<script src="desain_index/js/jquery.counterup.min.js"></script>
		<!-- Scroll Up JS-->
        <script src="desain_index/js/jquery.scrollUp.min.js"></script>
		<!-- Gmaps JS-->
		<script src="desain_index/js/gmaps.min.js"></script>
		<!-- Main JS-->
        <script src="desain_index/js/main.js"></script>
    </body>
</html>