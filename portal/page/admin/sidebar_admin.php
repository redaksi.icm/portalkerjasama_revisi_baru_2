<?php
include "../../config/koneksi.php";
$nama_admin = $_SESSION['nama_admin'];
$id_admin = $_SESSION['id_admin'];
$status = $_SESSION['status'];
if ($_SESSION['id_admin'] == null || $_SESSION['id_admin'] == 0) {
    header("location:login/error_admin.php");
} else {
}
?>
<div class="page-sidebar">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation">
        <li class="xn-logo">
            <a href="index.php">STMIK IP</a>
            <a href="#" class="x-navigation-control"></a>
        </li>
        <li class="xn-profile">
            <a href="#" class="profile-mini">
                <img src="../../gambar/logo2.jpg" alt="John Doe" />
            </a>
            <div class="profile">
                <div class="profile-image">
                    <img src="../../gambar/logo2.jpg" alt="John Doe" />
                </div>
                <div class="profile-data">
                    <div class="profile-data-name"><?php echo $_SESSION['nama_admin']; ?></div>
                    <div class="profile-data-title">Status User: <?php echo $status; ?></div>
                </div>
                <!-- <div class="profile-controls">
                    <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                    <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                </div> -->
            </div>
        </li>
        <li class="xn-title" style="color: white">Navigation</li>
        <li class="">
            <a href="index.php?admin=1"><span class="fa fa-desktop"></span> <span class="xn-text" style="color: white">Dashboard</span></a>
        </li>
        <?php if ($status == 'admin') { ?>
            <li><a href="index.php?admin=2"><span class="fa fa-file-text-o"></span> <span class="xn-text" style="color: white"> Permohonan Masuk</a></li>
            <li><a href="index.php?admin=5"><span class="fa fa-file-text-o"></span> <span class="xn-text" style="color: white"> permohonan Disetujui</a></li>
            <li><a href="index.php?admin=8"><span class="fa fa-file-text-o"></span> <span class="xn-text" style="color: white"> Pelaksanaan Kerjasama</a></li>
            <li><a href="index.php?admin=9"><span class="fa fa fa-comments"></span> <span class="xn-text" style="color: white"> Hasil dan Qusioner</a></li>
            <li><a href="index.php?admin=13"><span class="fa fa-file-text-o"></span> <span class="xn-text" style="color: white"> Kerjasama Luar Negri</a></li>
            <li><a href="index.php?admin=16"><span class="fa fa-comment-o"></span> <span class="xn-text" style="color: white"> Rata-rata jawaban</a></li>

        <?php } else { ?>
            <li><a href="index.php?admin=2"><span class="fa fa-file-text-o"></span> <span class="xn-text" style="color: white"> Permohonan Masuk</a></li>
            <li><a href="index.php?admin=5"><span class="fa fa-file-text-o"></span> <span class="xn-text" style="color: white"> Permohonan Disetujui</a></li>
            <li><a href="index.php?admin=8"><span class="fa fa-file-text-o"></span> <span class="xn-text" style="color: white"> Pelaksanaan Kerjasama</a></li>
            <li><a href="index.php?admin=9"><span class="fa fa fa-comments"></span> <span class="xn-text" style="color: white"> Hasil dan Qusioner</a></li>
            <li><a href="index.php?admin=13"><span class="fa fa-file-text-o"></span> <span class="xn-text" style="color: white"> Kerjasama Luar Negri</a></li>
            <li><a href="index.php?admin=16"><span class="fa fa-comment-o"></span> <span class="xn-text" style="color: white"> Rata-rata jawaban</a></li>

            <!-- <li><a href="index.php?admin=8"><span class="fa fa fa-comments"></span> <span class="xn-text"> Hasil dan Qusioner</a></li> -->
        <?php } ?>
        <li class="xn-title" style="color: white">Setting</li>
        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> <span class="xn-text" style="color: white"> Logout</a></li>

    </ul>
    <!-- END X-NAVIGATION -->
</div>

<!-- batas -->

<div class="page-content">
    <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
        <li class="xn-icon-button">
            <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
        </li>
        <li class="xn-icon-button pull-right">
            <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>
        </li>
        <!-- END SIGN OUT -->
        <!-- MESSAGES -->
        <li class="xn-icon-button pull-right">
            <a href="#"><span class="fa fa-comments"></span></a>
            <?php
            $query = mysqli_fetch_array(mysqli_query($connect, "SELECT count(id_proposal) as total_new FROM tb_proposal_mitra where status_proposal='terkirim'"));
            ?>
            <div class="informer informer-danger"><?php echo "$query[total_new]"; ?></div>
            <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                <div class="panel-heading">
                    <h3 class="panel-title"><span class="fa fa-comments"></span> Messages Proposal Kerjasama Belum di proses</h3>
                    <div class="pull-right">
                        <span class="label label-danger"><?php echo "$query[total_new]"; ?></span>
                    </div>
                </div>
                <div class="panel-body list-group list-group-contacts scroll" style="height: 200px;">
                    <?php
                    $tampilkan = mysqli_query($connect, "SELECT * FROM tb_proposal_mitra where status_proposal='terkirim' and status_proposal!='Di Setujui'  ORDER BY id_proposal DESC");
                    foreach ($tampilkan as $data) {

                    ?>
                        <a href="index.php?admin=2" class="list-group-item">
                            <div class="list-group-status status-online"></div>
                            <img src="../../gambar/logo.jpeg" class="pull-left" style="height: 50px; width: 50px" alt="John Doe" />
                            <span class="contacts-title">Nama Mitra: <?php echo $data['nama_mitra']; ?></span>
                            <p class="contacts-title">Tgl: <?php echo $data['tgl_pengajuan']; ?></p>
                            <p>Bidang: <?php echo $data['bidang_kerjasan']; ?></p>
                        </a>
                    <?php } ?>
                </div>
                <div class="panel-footer text-center">
                    <!-- <a href="pages-messages.html">Show all messages</a> -->
                </div>
            </div>
        </li>
    </ul>
    <ul class="breadcrumb">
    </ul>