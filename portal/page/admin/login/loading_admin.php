<?php
include "../../config/koneksi.php";

if (isset($_GET["admin"])) {
    $p = $_GET["admin"];
} else {
    $p = 1;
}
switch ($p) {

    case "1":
        require("page_admin/beranda.php");
        break;
    case "2":
        require("page_admin/data_proposal_masuk.php");
        break;
    case "3":
        require("page_admin/view_proposal_admin.php");
        break;

    case "4":
        require("page_admin/in_proses.php");
        break;

    case "5":
        require("page_admin/data_proposal_diterima.php");
        break;

    case "6":
        require("page_admin/catatan.php");
        break;

    case "7":
        require("page_admin/nomor_mou.php");
        break;

    case "8":
        require("page_admin/data_proposal_sedang_berjalan.php");
        break;

    case "9":
        require("page_admin/input_hasil_dan_qusioner.php");
        break;

    case "10";
        require("page_admin/kirim_qusiner.php");
        break;

    case "11";
        require("page_admin/hasil_qusioner.php");
        break;

    case "12";
        require("page_admin/input_hasil_kerjasama.php");
        break;

    case "13";
        require("page_admin/input_kerjasama_luar_negri.php");
        break;

    case "14";
        require("page_admin/input_kerjasama_luarnegri.php");
        break;

    case "15";
        require("page_admin/kirim_draf_mou.php");
        break;

    case "16";
        require("page_admin/analisa_jawaban.php");
        break;

    case "17";
        require("page_admin/hasil_kerjasama_utk_ketua.php");
        break;

    case "18";
        require("page_admin/edit_proposal_luarnegri.php");
        break;

    case "19";
        require("page_admin/hapuskerjasamaluarnegeri.php");
        break;

    case "20";
        require("page_admin/hapusdataditolak.php");
        break;

    case "21";
        require("page_admin/form_input_laporan.php");
        break;
}
