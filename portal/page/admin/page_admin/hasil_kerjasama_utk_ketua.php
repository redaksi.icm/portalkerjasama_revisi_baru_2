<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"> HASIL DAN KELANJUTAN KERJASAMA </h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>

                <?php
                $no = 1;

                $tampilkan = mysqli_query($connect, "SELECT * FROM tb_hasil_kerjasama where id_proposal='$_GET[id]'");

                foreach ($tampilkan as $data) {
                    $hasil = $data['hasil_kerjasama'];
                    $hasil2 = $data['kerjasama_selanjutnya'];
                }
                ?>

                <form action="" method="POST">
                    <div class="col-md-12">

                        <div class="block">
                            <h4> 1. Implementasi Kerjasama yang sudah terlaksana </h4>
                            <br>
                            <p><?php echo @$hasil; ?></p>
                            <!-- <textarea class="summernote" name="hasil_mou"><?php echo @$hasil; ?></textarea> -->
                        </div>

                        <div class="block">
                            <h4> 2. Perencanaan Kerjaasama Berikutnya</h4>
                            <br>
                            <p><?php echo @$hasil2; ?></p>
                            <!-- <textarea class="summernote" name="mou_selanjutnya"><?php echo @$hasil2; ?></textarea> -->
                        </div>
                        <div class="panel-footer">
                        <a href="index.php?admin=9" class="btn btn-primary pull-left">Kembali <span class="fa fa fa-mail-reply-all"></span></a>
                            
                        </div>

                    </div>
                </form>

            </div>
        </div>
    </div>