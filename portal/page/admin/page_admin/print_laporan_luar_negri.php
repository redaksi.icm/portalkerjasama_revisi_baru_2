<html>

<head>
    <title>Cetak Laporan</title>
    <style>
        /*design table 1*/
        .table1 {
            /* font-family: sans-serif; */
            color: #444;
            font-size: 12px;
            padding-right: 100px;
            border: 2px solid black;
            width: 80%;
        }

        .table1 tr th {
            background: #dbdbdb;
            color: #040303;
        }

        .table1,
        th,
        td {
            padding: 5px 2px;
            text-align: left;
            font-size: 12px;
        }

        .table1 tr:hover {
            background-color: #040303;
        }

        .table1 tr:nth-child(even) {
            background-color: ;
        }

        .div {
            border: 2px solid black;
            width: 100%;
            height: 10%;
        }
    </style>
</head>

<body onLoad="javascript:window.print()">
    <div class="">
        <table border="0" class="" align="center" style="width: 70%; padding-top: 50px;">
            <tr>
                <td rowspan="2" style="width: 220px">
                    <center> <img src="<?php echo ('../../../../portal/gambar/logo2.jpg'); ?>" style="width: 130px;height: 110px"></center>
                </td>
                <td valign="bottom" align="center">
                    <p style="text-align: center; font-size: 18px"><b>LAPORAN DATA KERJASAMA LUAR NEGRI</b></p>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                    <center> <b style="text-align: center;font-size: 18px">STMIK INDONESIA PADANG</b></center>
                </td>
            </tr>

        </table>
        <br><br><br>
        <table border="0" class="table1" align="center">
            <thead>
                <tr>
                    <th>NO</th>
                    <th>Lembaga Mitra Kerjasama</th>
                    <th>Bidang Kerjaama</th>
                    <th>Masa Berlaku </th>
                    <th>Implementasi Kerjasama </th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                include "../../../config/koneksi.php";
                $tampilkan = mysqli_query($connect, "SELECT * FROM tb_kerjasama_luar_negri ORDER BY id_proposal_luar_negri DESC");
                foreach ($tampilkan as $data) {

                ?>
                    <tr>
                        <td><?php echo $no++ ?></td>
                        <td><?php echo $data['lembaga_mitra']; ?></td>
                        <td><?php echo $data['bidang_kerja_sama']; ?></td>
                        <td><?php echo $data['masa_berlaku']; ?></td>
                        <td><?php echo $data['hasil_kerjasama']; ?></td>
                    </tr>
                <?php } ?>
            </tbody>

        </table>
        <br>
        <br>
        <br>
        <center>
            <table border="0" style="text-align: center">
                <tr>
                    <td style="width: 150px;">
                        <div style="padding-left: 600px">Padang, <?php echo date("d-m-Y"); ?></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="padding-left: 630px">Ketua</div>
                        <div style="padding-left: 600px">STMIK INDONESIA</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <div style="padding-left: 590px">Prof.Dr,-Ing.Ir. H. Hairul Abral</div>
                        <div style="padding-left: 590px">Nip 196608171992121001 </div>
                    </td>
                </tr>
            </table>
        </center>
    </div>
</body>

</html>