<?php
include "../../config/koneksi.php";
if (isset($_POST['simpan'])) {
    $nama_mitra = $_POST['nama_mitra'];
    $bidang = $_POST['bidang'];
    $masaberlaku = $_POST['masaberlaku'];
    $hasil_mou = $_POST['hasil_mou'];
    // $file_proposal = $_POST['file_proposal'];
    $kosong = $ukuran    = $_FILES['file_mou']['size'];
    if ($kosong == 0) {
        $ekstensi_diperbolehkan = array('pdf', 'pdf');
        $nama = 'datakosong.pdf';
        $x = explode('.', $nama);
        $ekstensi = strtolower(end($x));
        $nama = 'proposal_kosong.pdf';
    } else {

        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        $kode_random = substr(str_shuffle($permitted_chars), 0, 5);

        $ekstensi_diperbolehkan = array('pdf', 'pdf');
        $nama1 = $_FILES['file_mou']['name'];
        $nama = $kode_random . '-' . $nama1;
        $x = explode('.', $nama);
        $ekstensi = strtolower(end($x));
        $ukuran    = $_FILES['file_mou']['size'];
        $file_tmp = $_FILES['file_mou']['tmp_name'];
    }

    if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
        if ($kosong == 0) {
        } else {
            move_uploaded_file($file_tmp, 'file_luar_negri/' . $nama);
        }
        // $simpan = mysqli_query($connect, "INSERT INTO tb_kerjasama_luar_negri (lembaga_mitra, bidang_kerja_sama, masa_berlaku, hasil_kerjasam, file_proposal) VALUES ('$nama_mitra','$bidang','$masaberlaku','$nama')");

        $simpan12 = mysqli_query($connect, "INSERT INTO tb_kerjasama_luar_negri (lembaga_mitra, bidang_kerja_sama, masa_berlaku, hasil_kerjasama, file_mou) VALUES ('$nama_mitra','$bidang','$masaberlaku','$hasil_mou','$nama')");

        if (!$simpan12) {
            echo '<div class="col-md-12">
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Proses Gagal!</strong> Terdapat kesalahan dalam penyimpanan data.
                    </div>  
                </div>';
        } else {

            echo '<div class="col-md-12">
                <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>

                    </button>
                    <strong>Sukses!</strong> Data berhasil di simpan <a href="index.php?admin=13" class="btn btn-danger btn-sm">lihat data</a>
                </div>
            </div>';
        }
    } else {

        echo '<div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
                </button>
                <strong>Proses Gagal!</strong> File harus berjenis .PDF
            </div>  
        </div>';
    }
}
?>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">INPUT DATA KERJASAMA LUAR NEGRI</h3>
                </div>

                <form action="" method="POST" enctype="multipart/form-data">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4  control-label">Lembaga Mitra Kerjasama : </label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" class="form-control" name="nama_mitra">
                                        </div>
                                        <span class="help-block">.</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4  control-label">Bidang : </label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" class="form-control" name="bidang">
                                        </div>
                                        <span class="help-block">.</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4  control-label">Masa Berlaku : </label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <select class="form-control select" data-live-search="true" name="masaberlaku">
                                                <option value="0000">Pilih Tahun</option>
                                                <?php
                                                date_default_timezone_set('Asia/Jakarta');
                                                $tahunow = '2060';
                                                for ($tahun = 1996; $tahun <= $tahunow; $tahun++) {
                                                    echo "<option  value='$tahun'>$tahun </option>";
                                                }

                                                ?>
                                            </select>
                                        </div>
                                        <span class="help-block">.</span>
                                    </div>
                                </div>
                            </div>
                            <div class='col-md-6'>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">File MoU : </label>
                                    <div class="col-md-9">
                                        <input type="file" class="fileinput btn-primary" name="file_mou" id="file_mou" title="Browse file">
                                        <span class="help-block">File berbentuk .PDF</span>
                                        <br>

                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Manfaat Kerjasama : </label>
                                    <div class="col-md-12">
                                        <textarea class="summernote" name="hasil_mou"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="form-group">
                                    <button class="btn btn-primary pull-left" name="simpan">SAVE<span class="fa fa-floppy-o fa-right"></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>