<?php
include "../../config/koneksi.php";
$nama_admin = $_SESSION['nama_admin'];
$id_admin = $_SESSION['id_admin'];
$status = $_SESSION['status'];
if ($_SESSION['id_admin'] == null || $_SESSION['id_admin'] == 0) {
    header("location:login/error_admin.php");
} else {
}
?>
<div class="page-content-wrap">
    <!-- START WIDGETS -->
    <div class="row">

        <div class="col-md-12">

            <!-- START WIDGET CLOCK -->
            <div class="widget widget-info widget-padding-sm">
                <div class="widget-big-int plugin-clock">00:00</div>
                <div class="widget-subtitle plugin-date">Loading...</div>
                <div class="widget-controls">
                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="left" title="Remove Widget"><span class="fa fa-times"></span></a>
                </div>
                <div class="widget-buttons widget-c3">
                    <div class="col">
                        <a href="#"><span class="fa fa-clock-o"></span></a>
                    </div>
                    <div class="col">
                        <a href="#"><span class="fa fa-bell"></span></a>
                    </div>
                    <div class="col">
                        <a href="#"><span class="fa fa-calendar"></span></a>
                    </div>
                </div>
            </div>
            <!-- END WIDGET CLOCK -->

        </div>
        <!-- <div class="col-md-12 scCol ui-sortable">

            <div class="panel panel-success" id="grid_block_5">
                <div class="panel-heading ui-draggable-handle ui-sortable-handle">
                    <h3 class="panel-title">Selamat Datang...</h3>
                </div>
                <div class="panel-body">
                    STMIK INDONESIA PADANG
                </div>
            </div>

        </div> -->
    </div>
    <!-- END WIDGETS -->

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Kepuasan Mitra Kerjasama</h3>
                </div>
                <div class="panel-body">
                    <div id="donut-chart" style="height: 300px;"></div>
                    <div class="timeline-footer">
                        <a class="btn btn-danger btn-xs">Tidak Puas</a>
                        <a class="btn btn-danger btn-xs" style="background-color: #f39c12;">Puas</a>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Grafik Pilihan Bidang Kerjasama</h3>
                </div>
                <div class="panel-body">
                    <div id="bar-chart" style="height: 300px; width:800px;"></div>
                    
                    <p>KETERANGAN</p>
                    <p>1. bidang pendidikan dan pengajaran</p>  
                    <p>2. bidang penelitian dan pengembangan ilmu</p>   
                    <p>3. bidang pengabdian kepada masyarakat</p>  
                    <p>4. bidang kegiatan lainnya</p>
                </div>

            </div>

        </div>
        <div class="col-md-4">

            <!-- START VISITORS BLOCK -->

            <!-- END VISITORS BLOCK -->

        </div>

        <div class="col-md-4">

            <!-- START PROJECTS BLOCK -->
            <!-- END PROJECTS BLOCK -->

        </div>
    </div>

    <div class="row">
        <div class="col-md-8">

            <!-- START SALES BLOCK -->
            <!-- END SALES BLOCK -->

        </div>
        <div class="common-modal modal fade" id="common-Modal1" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-content">
                <ul class="list-inline item-details">
                    <li><a href="http://themifycloud.com/downloads/janux-premium-responsive-bootstrap-admin-dashboard-template/">Admin templates</a></li>
                    <li><a href="http://themescloud.org">Bootstrap themes</a></li>
                </ul>
            </div>
        </div>

        <div class="col-md-4">

            <!-- START SALES & EVENTS BLOCK -->

            <!-- END SALES & EVENTS BLOCK -->

        </div>
    </div>

    <!-- START DASHBOARD CHART -->
    <div class="chart-holder" id="dashboard-area-1" style="height: 200px;"></div>
    <div class="block-full-width">

    </div>
    <!-- END DASHBOARD CHART -->

</div>