<?php
include "../../config/koneksi.php";
$nama_admin = $_SESSION['nama_admin'];
$id_admin = $_SESSION['id_admin'];
$status = $_SESSION['status'];
if ($_SESSION['id_admin'] == null || $_SESSION['id_admin'] == 0) {
    header("location:login/error_admin.php");
} else {
}
?>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">HASIL DAN QUSIONER</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <form target="_blank" action="page_admin/print_laporan.php">
                                <div class="form-group">
                                    <label class="control-label">BUAT LAPORAN</label>
                                    <br>
                                    <div class="col-md-3">
                                        <select class="" style="width: 100px;height: 30px;" name="tahun">
                                            <option value="0">Pilih Tahun..</option>
                                            <?php
                                            date_default_timezone_set('Asia/Jakarta');
                                            $tahunow = date('Y');
                                            for ($tahun = 2015; $tahun <= $tahunow; $tahun++) {
                                                echo "<option  value='$tahun'>$tahun </option>";
                                            }

                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select class="" style="width: 100px;height: 30px;" name="bulan">
                                            <option value="0">Pilih Bulan..</option>
                                            <option value="1">Januari</option>
                                            <option value="2">Februari</option>
                                            <option value="3">Maret</option>
                                            <option value="4">April</option>
                                            <option value="5">Mei</option>
                                            <option value="6">Juni</option>
                                            <option value="7">Juli</option>
                                            <option value="8">Agustus</option>
                                            <option value="9">September</option>
                                            <option value="10">Oktobet</option>
                                            <option value="11">November</option>
                                            <option value="12">Desember</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <button class="btn btn-primary pull-left">Print</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <br>
                <br>

                <div class="panel-body">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>Action</th>
                                <th>No Mou STMIK</th>
                                <th>No Mou Mitra</th>
                                <th>File Laporan</th>
                                <th>Nama Instansi</th>
                                <th>Tgl TTD</th>
                                <th>Tgl Pengajuan</th>
                                <th>Masa Berlaku</th>
                                <th>Nama Mitra</th>
                                <th>Status Qusioner</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampilkan = mysqli_query($connect, "SELECT tb_proposal_mitra.*, tb_file_mou.* FROM tb_proposal_mitra INNER JOIN tb_file_mou on tb_file_mou.id_proposal=tb_proposal_mitra.id_proposal where tb_proposal_mitra.status_proposal='Di Setujui' and tb_proposal_mitra.status_qusioner = 'Selesai' OR tb_proposal_mitra.status_qusioner = 'Terkirim'  ORDER BY tb_proposal_mitra.id_proposal DESC");

                            foreach ($tampilkan as $data) {
                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td>

                                        <div class="btn-group">
                                            <a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false">Action <span class="caret"></span></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <?php if ($status == 'admin') { ?>
                                                    <li><a href="index.php?admin=3&id=<?php echo $data['id_proposal']; ?>&session=qusioner">View Data</a></li>
                                                    <li><a href="index.php?admin=12&id=<?php echo $data['id_proposal']; ?>&session=pesan">Input Hasil Kerjasama</a></li>
                                                    <li><a href="index.php?admin=21&id=<?php echo $data['id_proposal']; ?>">Input Laporan</a></li>
                                                    <li><a href="index.php?admin=11&id=<?php echo $data['id_proposal']; ?>&session=qusioner">Hasil Qusioner</a></li>
                                                    
                                                <?php } else { ?>
                                                    <li><a href="index.php?admin=3&id=<?php echo $data['id_proposal']; ?>&session=qusioner">View Data</a></li>
                                                    <li><a href="index.php?admin=17&id=<?php echo $data['id_proposal']; ?>&session=pesan">Hasil Kerjasama</a></li>
                                                    
                                                    <li><a href="index.php?admin=11&id=<?php echo $data['id_proposal']; ?>&session=qusioner">Hasil Qusioner</a></li>
                                                <?php } ?>
                                            </ul>
                                        </div>



                                    </td>
                                    <td><a href="file_mou/<?php echo $data['file_mou_stmik']; ?>" target="_blank"><?php echo $data['no_mou']; ?></a></td>
                                    <td><a href="file_mou/<?php echo $data['file_mou_stmik']; ?>" target="_blank"><?php echo $data['no_mou_mitra']; ?></a></td>
                                    <td>
                                        <?php

                                        if (empty($data['laporan_kerja_sama'])) {
                                            echo 'Data Kosong';
                                        } else {
                                            echo '<a href="file_laporan/' . $data["laporan_kerja_sama"] . '" target="_blank">Download</a>';
                                        }
                                        ?>

                                    </td>
                                    <td><?php echo $data['nama_instansi']; ?></td>
                                    <td><?php echo $data['tgl_mou']; ?></td>
                                    <td><?php echo $data['tgl_pengajuan']; ?></td>
                                    <td><?php echo $data['masa_berlaku']; ?></td>
                                    <td><?php echo $data['nama_mitra']; ?></td>
                                    <td><?php if ($data['status_qusioner'] == 'Terkirim') { ?>
                                            Belum di isi
                                        <?php } elseif ($data['status_qusioner'] == 'Selesai') { ?>
                                            Sudah dI Isi
                                        <?php } else { ?>
                                            Belum di terima
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>