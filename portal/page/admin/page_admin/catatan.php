<?php
if (isset($_POST['simpan'])) {
    $catatan = $_POST['catatan'];
    $q = mysqli_query($connect, "SELECT * from tb_catatan where id_proposal ='$_GET[id]'");
    $row = mysqli_fetch_array($q);

    if (mysqli_num_rows($q) == 1) {

        $updatekan = mysqli_query($connect, "UPDATE tb_catatan SET  catatan='$catatan', status='terkirim' where id_proposal ='$_GET[id]'");
        if ($connect) {
            echo '<div class="col-md-12">
                <div class="alert alert-success" role="alert">
                <a href="index.php?admin=5" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>                    
                    </a>
                    <strong>Sukses!</strong> Pesan sudah diupdate
                </div>
            </div>';
        } else {
            echo '<div class="col-md-12">
                <div class="alert alert-danger" role="alert">
                    <a href="index.php?admin=5" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    </a>
                    <strong>Proses Gagal!</strong> Pesan Tidak Terupdate.
                </div>  
            </div>';
        }
    } else {
        $simpan = mysqli_query($connect, "INSERT INTO tb_catatan (id_proposal,catatan,status,id_user) VALUES ('$_GET[id]','$catatan','terkirim','$_POST[id_user]')");
        if (!$simpan) {
            echo '<div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <a href="index.php?admin=5" class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
                </a>
                <strong>Proses Gagal!</strong> Pesan Tidak Terkirim.
            </div>  
        </div>';
        } else {
            echo '<div class="col-md-12">
        <div class="alert alert-success" role="alert">
        <a href="index.php?admin=5" class="close" data-dismiss="alert">
            <span aria-hidden="true">&times;</span>                    
            </a>
            <strong>Sukses!</strong> Pesan sudah dikirim <a href="index.php?admin=5" class="btn btn-danger btn-sm">Lihat Proposal</a>
        </div>
    </div>';
        }
    }
}
?>

<?php
$no = 1;
$tampilkan = mysqli_query($connect, "SELECT tb_proposal_mitra.*, tb_catatan.catatan FROM tb_proposal_mitra LEFT JOIN tb_catatan on tb_proposal_mitra.id_proposal=tb_catatan.id_proposal where tb_proposal_mitra.id_proposal = '$_GET[id]'");
foreach ($tampilkan as $data) {
    // while ($data = mysqli_fetch_array($tampilkan)){
?>

    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-colorful">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <h2><span class="fa fa fa-envelope"></span> KIRIM CATATAN KE MITRA</h2>
                        </h3>
                    </div>
                    <form action="" method="POST">
                        <div class="col-md-8">
                            <div class="">
                                <div class="panel-body">
                                    <h2>Nama Mitra : <?php echo $data['nama_mitra']; ?></h2>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ID: PROPOSAL</label>
                                            <input type="text" class="form-control" value="<?php echo $data['id_proposal']; ?>">
                                            <input type="hidden" class="form-control" name="id_user" value="<?php echo $data['id_user']; ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>NAMA INSTANSI</label>
                                            <input type="text" class="form-control" value="<?php echo $data['nama_instansi']; ?>">
                                        </div>
                                        <br>
                                    </div>
                                    <div class="form-group">
                                        <label>TANGGAL PENGAJUAN</label>
                                        <input type="text" class="form-control" value="<?php echo $data['tgl_pengajuan']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>CATATAN</label>
                                        <textarea class="form-control" name="catatan" placeholder="Your message" rows="3"><?php echo $data['catatan']; ?></textarea>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <a href="index.php?admin=5" class="btn btn-primary pull-left">Kembali <span class="fa fa fa-mail-reply-all"></span></a>
                                    <button class="btn btn-success pull-right" type="submit" name="simpan">KIRIM</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>