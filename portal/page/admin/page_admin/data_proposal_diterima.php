<?php
include "../../config/koneksi.php";
$nama_admin = $_SESSION['nama_admin'];
$id_admin = $_SESSION['id_admin'];
$status = $_SESSION['status'];
if ($_SESSION['id_admin'] == null || $_SESSION['id_admin'] == 0) {
    header("location:login/error_admin.php");
} else {
}
?>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">PERMOHONAN KERJASAMA DISETUJUI</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>Action</th>
                                <th>Nama Instansi</th>
                                <th>Nama Mitra</th>                                                                
                                <th>Jabatan</th>                                
                                <th>Tgl Pengajuan</th>
                                <th>Bidang Kerjasama</th>
                                <th>File Proposal</th>
                                <th>Kategori</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                           // $tampilkan = mysqli_query($connect, "SELECT tb_proposal_mitra.*, tb_draf_mou.file_draf_mou_mitra,tb_draf_mou.logo_mitra FROM tb_proposal_mitra LEFT JOIN tb_draf_mou on tb_draf_mou.id_proposal=tb_proposal_mitra.id_proposal where tb_proposal_mitra.status_proposal='Di Setujui'  and tb_proposal_mitra.no_mou = '-' ORDER BY tb_proposal_mitra.id_proposal DESC");
                            $tampilkan = mysqli_query($connect, "SELECT tb_proposal_mitra.*, tb_draf_mou.file_draf_mou_stmik FROM tb_proposal_mitra LEFT JOIN tb_draf_mou on tb_draf_mou.id_proposal=tb_proposal_mitra.id_proposal where tb_proposal_mitra.status_proposal='Di Setujui'  and tb_proposal_mitra.no_mou = '-' ORDER BY tb_proposal_mitra.id_proposal DESC");
                            foreach ($tampilkan as $data) {

                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td>
                                        <?php if ($status == 'admin') { ?>
                                            <div class="btn-group">
                                                <a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false">Action <span class="caret"></span></a>
                                                <ul class="dropdown-menu" role="menu">                                                   
                                                    <li><a href="index.php?admin=3&id=<?php echo $data['id_proposal']; ?>&session=hidden">View Data</a></li>
                                                    <!--<li><a href="index.php?admin=6&id=<?php echo $data['id_proposal']; ?>">Kirim Pesan</a></li> 
                                                    <?php if(strlen($data['file_draf_mou_mitra']) > 0){ ?>   
                                                    <li><a target="_blank" href="file_draf_mou/<?php echo $data['file_draf_mou_mitra']; ?>">View draf mou mitra</a></li>    
                                                    <li><a target="_blank" href="logo_mou/<?php echo $data['logo_mitra']; ?>">View Logo mou mitra</a></li> 
                                                    <?php }else{?>-->
                                                        
                                                    <?php }?> 
                                                    <li><a href="index.php?admin=15&id=<?php echo $data['id_proposal']; ?>">Kirim draf mou ke mitra</a></li>                                                
                                                    <li><a href="index.php?admin=7&id=<?php echo $data['id_proposal']; ?>">Input mou</a></li>                                                   
                                                    <!-- <li><a href="index.php?admin=4&id=<?php echo $data['id_proposal']; ?>&Proses=Di_tolak">Delete</a></li>                                                    -->
                                                </ul>
                                            </div>
                                        <?php } else { ?>
                                            <a title="View Propoasal" href="index.php?admin=3&id=<?php echo $data['id_proposal']; ?>&session=hidden" class="btn-info btn-rounded btn-sm"><span class="glyphicon glyphicon-eye-open"></span></a>
                                        <?php } ?>
                                    </td>
                                    <td><?php echo $data['nama_instansi']; ?></td>
                                    <td><?php echo $data['nama_mitra']; ?></td>
                                    <td><?php echo $data['jabatan']; ?></td>                                    
                                    <td><?php echo $data['tgl_pengajuan']; ?></td>
                                    <td><?php echo $data['bidang_kerjasan']; ?></td>
                                    <td>
                                        <a title="View Data Proposal Pengajuan Kerjasama" target="_blank" href="../mitra/file/<?php echo $data['file_proposal']; ?>" class="btn btn-info"><i class="fa fa-folder-open"></i></a>
                                    </td>
                                    <td><?php echo $data['kategori_proposal']; ?></td>
                                    <td><?php if ($data['status_proposal'] == 'Terkirim') {
                                            echo 'Belum Proses';
                                        } else {
                                            echo $data['status_proposal'];
                                        }; ?></td>

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <br>
            <br>
            <!-- <i>Info.</i><br>
            <i>1. Info akan diberikan kembali dalam waktu 2 hari, untuk status proposal di proses..</i><br>
            <i>2. Admin kami akan menghubungi anda jika proposal kerjasama di sepakati. dengan jangka waktu 2 hari.</i><br>
            <i>3. Pending = anda belum melakukan pengiriman proposal dengan mencentang (data sudah lengkap).</i><br>
            <i>4. Terkirim = anda sudah melakukan pengiriman proposal.</i><br>
            <i>5. Di Tolak = Proposal andan di tolak (dengan alasan tertentu).</i> -->
        </div>
    </div>
</div>