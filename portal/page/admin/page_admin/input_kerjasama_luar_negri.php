<?php
include "../../config/koneksi.php";
$nama_admin = $_SESSION['nama_admin'];
$id_admin = $_SESSION['id_admin'];
$status = $_SESSION['status'];
if ($_SESSION['id_admin'] == null || $_SESSION['id_admin'] == 0) {
    header("location:login/error_admin.php");
} else {
}
?>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">REKAP KERJASAMA LUAR NEGRI</h3>
                    <ul class="panel-controls">
                        <?php if ($status == 'admin') { ?>
                            <div class="pull-left" style="width: 200px;">
                                <a href="index.php?admin=14" class="btn btn-info btn-block"><span class="fa fa-plus"></span> TAMBAH DATA</a>
                            </div>
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                            <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                        <?php } else { ?>
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                            <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <br>
                <br>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <form target="_blank" action="page_admin/print_laporan_luar_negri.php">
                                <div class="form-group">
                                    <label class="control-label">BUAT LAPORAN</label>
                                    <br>
                                    <!-- <div class="col-md-3">
                                        <select class="" style="width: 100px;height: 30px;" name="tahun">
                                            <option value="0">Pilih Tahun..</option>
                                            <?php
                                            date_default_timezone_set('Asia/Jakarta');
                                            $tahunow = date('Y');
                                            for ($tahun = 2015; $tahun <= $tahunow; $tahun++) {
                                                echo "<option  value='$tahun'>$tahun </option>";
                                            }

                                            ?>
                                        </select>
                                    </div> -->
                                    <!-- <div class="col-md-3">
                                        <select class="" style="width: 100px;height: 30px;" name="bulan">
                                            <option value="0">Pilih Bulan..</option>
                                            <option value="1">Januari</option>
                                            <option value="2">Februari</option>
                                            <option value="3">Maret</option>
                                            <option value="4">April</option>
                                            <option value="5">Mei</option>
                                            <option value="6">Juni</option>
                                            <option value="7">Juli</option>
                                            <option value="8">Agustus</option>
                                            <option value="9">September</option>
                                            <option value="10">Oktobet</option>
                                            <option value="11">November</option>
                                            <option value="12">Desember</option>
                                        </select>
                                    </div> -->
                                    <div class="col-md-6">
                                        <button class="btn btn-primary pull-left">Print</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <br>
                <br>
                <div class="panel-body">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>Action</th>
                                <th>Lembaga Mitra Kerjasama</th>
                                <th>Bidang Kerjaama</th>
                                <th>Masa Berlaku </th>
                                <th>Manfaat Kerjasama </th>
                                <th>File MoU</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampilkan = mysqli_query($connect, "SELECT * FROM tb_kerjasama_luar_negri ORDER BY id_proposal_luar_negri DESC");
                            foreach ($tampilkan as $data) {

                            ?>
                                <tr>
                                    <td><?php echo $no++ ?></td>
                                    <td> <?php if ($status == 'admin') { ?>
                                            <div class="btn-group">
                                                <a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false">Action <span class="caret"></span></a>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="index.php?admin=18&id=<?php echo $data['id_proposal_luar_negri']; ?>&session=hidden">Edit</a></li>
                                                    <li><a href="index.php?admin=19&id=<?php echo $data['id_proposal_luar_negri']; ?>">Hapus</a></li>
                                                    
                                                </ul>
                                            </div>
                                        <?php } else { ?>
                                            -
                                        <?php } ?>
                                    </td>
                                    <td><?php echo $data['lembaga_mitra']; ?></td>
                                    <td><?php echo $data['bidang_kerja_sama']; ?></td>
                                    <td><?php echo $data['masa_berlaku']; ?></td>
                                    <td><?php echo $data['hasil_kerjasama']; ?></td>
                                    <td><a title="View Data Proposal Pengajuan Kerjasama" target="_blank" href="file_luar_negri/<?php echo $data['file_mou']; ?>" class="btn btn-info"><i class="fa fa-folder-open"></i></a></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>