<html>

<head>
    <title>Cetak Laporan</title>
    <style>
        /*design table 1*/
        .table1 {
            /* font-family: sans-serif; */
            color: #444;
            font-size: 12px;
            padding-right: 100px;
            border: 2px solid black;
            width: 80%;
        }

        .table1 tr th {
            background: #dbdbdb;
            color: #040303;
        }

        .table1,
        th,
        td {
            padding: 5px 2px;
            text-align: left;
            font-size: 12px;
        }

        .table1 tr:hover {
            background-color: #040303;
        }

        .table1 tr:nth-child(even) {
            background-color: ;
        }

        .div {
            border: 2px solid black;
            width: 100%;
            height: 10%;
        }
    </style>
</head>

<body onLoad="javascript:window.print()">
    <div class="">
        <table border="0" class="" align="center" style="width: 70%; padding-top: 50px;">
            <tr>
                <td rowspan="2" style="width: 220px">
                    <center> <img src="<?php echo ('../../../../portal/gambar/logo2.jpg'); ?>" style="width: 130px;height: 110px"></center>
                </td>
                <td valign="bottom" align="center">
                    <p style="text-align: center; font-size: 18px"><b>LAPORAN DATA KERJASAMA DALAM NEGRI</b></p>
                </td>
               
            </tr>
            <tr>
                <td valign="top" align="center">
                    <center> <b style="text-align: center;font-size: 18px">STMIK INDONESIA PADANG</b></center>
                </td>
            </tr>

        </table>
        <br><br><br>
        <table border="0" class="table1" align="center">
            <thead>
                <tr>
                    <th>NO</th>
                    <th>Nama Instansi</th>
                    <th>Tgl TTD</th>
                    <th>Tgl Pengajuan</th>
                    <th>Masa Berlaku</th>
                    <th>Nama Mitra</th>
                    <th>Manfaat Kerjasama</th>
                    <th>kerjasama selanjutnya</th>
                    <!--<th>Status Qusioner</th>-->
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                include "../../../config/koneksi.php";
                $tahun = $_GET['tahun'];
                $bulan = $_GET['bulan'];
                $tampilkan = mysqli_query($connect, "SELECT tb_proposal_mitra.*, tb_file_mou.*,tb_hasil_kerjasama.* FROM tb_proposal_mitra INNER JOIN tb_file_mou on tb_file_mou.id_proposal=tb_proposal_mitra.id_proposal INNER JOIN tb_hasil_kerjasama on tb_hasil_kerjasama.id_proposal=tb_proposal_mitra.id_proposal where tb_proposal_mitra.status_proposal='Di Setujui' and tb_proposal_mitra.status_qusioner = 'Selesai' and month(tgl_pengajuan)='$bulan' and year(tgl_pengajuan)='$tahun'   ORDER BY tb_proposal_mitra.id_proposal DESC");

                foreach ($tampilkan as $data) {
                ?>
                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $data['nama_instansi']; ?></td>
                        <td><?php echo $data['tgl_mou']; ?></td>
                        <td><?php echo $data['tgl_pengajuan']; ?></td>
                        <td><?php echo $data['masa_berlaku']; ?></td>
                        <td><?php echo $data['nama_mitra']; ?></td>
                        <td><?php echo $data['hasil_kerjasama']; ?></td>
                        <td><?php echo $data['kerjasama_selanjutnya']; ?></td>
                        <!--<td><?php if ($data['status_qusioner'] == 'Terkirim') { ?>
                                Belum di isi
                            <?php } elseif ($data['status_qusioner'] == 'Selesai') { ?>
                                Sudah dI Isi
                            <?php } else { ?>
                                Belum di terima
                            <?php } ?>-->
                        </td>
                    </tr>
                <?php } ?>
            </tbody>

        </table>
        <br>
        <br>
        <br>
        <center>
            <table border="0" style="text-align: center">
                <tr>
                    <td style="width: 150px;">
                        <div style="padding-left: 500px">Padang, <?php echo date("d-m-Y"); ?></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="padding-left: 500px">Ketua </div>
                        <div style="padding-left: 500px">STMIK INDONESIA</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <div style="padding-left: 500px">Prof.Dr,-Ing.Ir. H. Hairul Abral</div>
                        <div style="padding-left: 500px">Nip 196608171992121001 </div>
                    </td>
                </tr>
            </table>
        </center>
    </div>
</body>

</html>