<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">RATA - RATA JAWABAN QUSIONER</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="col-md-12">
                        Keterangan pengisian <br>
                        <!-- <i>Berilah tanda √ pada koloom 1 s.d 5 sesuai penilaian Bapak / Ibu, dangan keterangan sebagai
                            berikut <br> -->
                        1 Kurang Sekali <br>
                        2 Kurang <br>
                        3 Sedang <br>
                        4 Baik <br>
                        5 Baik Sekali</i><br>
                        <br>
                    </div>
                    <h1>RATA - RATA JAWABAN MITRA</h1>
                    <br>
                    <?php
                    $query1 = mysqli_fetch_array(mysqli_query($connect, "SELECT count(*)  as total_1 FROM tb_jawaban where jawaban = 1"));
                    $query2 = mysqli_fetch_array(mysqli_query($connect, "SELECT count(*)  as total_2 FROM tb_jawaban where jawaban = 2"));
                    $query3 = mysqli_fetch_array(mysqli_query($connect, "SELECT count(*)  as total_3 FROM tb_jawaban where jawaban = 3"));
                    // batas

                    $query4 = mysqli_fetch_array(mysqli_query($connect, "SELECT count(*)  as total_4 FROM tb_jawaban where jawaban = 4"));
                    $query5 = mysqli_fetch_array(mysqli_query($connect, "SELECT count(*)  as total_5 FROM tb_jawaban where jawaban = 5"));
                    ?>
                    <h3>Kesimpulan Jawaban: <?php
                                            $total1 = $query1['total_1'] + $query2['total_2'] + $query3['total_3'];
                                            $total2 = $query4['total_4'] + $query5['total_5'];

                                            if ($total2 > $total1) {
                                                echo '<b  style="color: brown;">PUAS</b>';
                                            } else {
                                                echo '<b  style="color: brown;">TIDAK PUAS</b>';
                                            }
                                            ?></h3>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <!-- <th>ID</th> -->
                                <th>Kategori</th>
                                <th>1</th>
                                <th>2</th>
                                <th>3</th>
                                <th>4</th>
                                <th>5</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampilkan = mysqli_query($connect, "SELECT tb_pertanyaan_kusioner.kategori_pertanyaan, tb_pertanyaan_kusioner.id_pertanyaan  FROM tb_pertanyaan_kusioner LEFT JOIN tb_jawaban on tb_pertanyaan_kusioner.id_pertanyaan = tb_jawaban.id_pertanyaan group by tb_pertanyaan_kusioner.kategori_pertanyaan, tb_pertanyaan_kusioner.id_pertanyaan");
                            foreach ($tampilkan as $data) {

                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <!-- <td><?php echo $data['id_pertanyaan'] ?></td> -->
                                    <td><?php echo $data['kategori_pertanyaan']; ?></td>
                                    <td>

                                        <?php
                                        $query = mysqli_fetch_array(mysqli_query($connect, "SELECT count(*) as total_new FROM tb_jawaban where id_pertanyaan='$data[id_pertanyaan]' and jawaban = 1"));

                                        echo "$query[total_new]";
                                        ?>

                                    </td>
                                    <td>
                                        <?php
                                        $query = mysqli_fetch_array(mysqli_query($connect, "SELECT count(*) as total_new FROM tb_jawaban where id_pertanyaan='$data[id_pertanyaan]' and jawaban = 2"));

                                        echo "$query[total_new]";
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $query = mysqli_fetch_array(mysqli_query($connect, "SELECT count(*) as total_new FROM tb_jawaban where id_pertanyaan='$data[id_pertanyaan]' and jawaban = 3"));

                                        echo "$query[total_new]";
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $query = mysqli_fetch_array(mysqli_query($connect, "SELECT count(*) as total_new FROM tb_jawaban where id_pertanyaan='$data[id_pertanyaan]' and jawaban = 4"));

                                        echo "$query[total_new]";
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $query = mysqli_fetch_array(mysqli_query($connect, "SELECT count(*) as total_new FROM tb_jawaban where id_pertanyaan='$data[id_pertanyaan]' and jawaban = 5"));

                                        echo "$query[total_new]";
                                        ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <div class="panel-footer">
                             <a href="index.php?admin=1" class="btn btn-primary pull-left">Kembali <span class="fa fa fa-mail-reply-all"></span></a>
                            <!-- <button class="btn btn-default">Clear Form</button> -->
                            <!-- <button class="btn btn-primary pull-right" name="simpan" type="submit">Simpan</button> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>