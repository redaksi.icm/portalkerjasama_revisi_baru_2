<?php
if (isset($_POST['simpan'])) {

    $q = mysqli_query($connect, "SELECT * from tb_draf_mou where id_proposal ='$_GET[id]'");
    $row = mysqli_fetch_array($q);

    if (mysqli_num_rows($q) == 1) {
        echo '<div class="col-md-12">
                <div class="alert alert-danger" role="alert">
                    <a href="index.php?admin=15&id=' . $_GET["id"] . '" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    </a>
                    <strong>Proses Gagal!</strong>Draf sudah ada dalam sistem
                </div>  
            </div>';
    } else {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        $kode_random = substr(str_shuffle($permitted_chars), 0, 5);

        // draf
        $ekstensi_diperbolehkan_draf = array('pdf');
        $nama_draf = $_FILES['file_draf_mou']['name'];
        $draf = $kode_random . '-' . date('Y-m-d') . '-' . $nama_draf;
        $nama_file_db = str_replace('/', '-', $draf);
        $x_draf = explode('.', $draf);
        $ekstensi_draf = strtolower(end($x_draf));
        $ukuran_draf    = $_FILES['file_draf_mou']['size'];
        $file_tmp_draf = $_FILES['file_draf_mou']['tmp_name'];

        //  untuk logo
       // $ekstensi_diperbolehkan_logo = array('png', 'jpg', 'jpeg');
       // $nama_logo = $_FILES['logo_mou']['name'];
       // $logo = $kode_random . '-' . date('Y-m-d') . '-' . $nama_logo;
       // $nama_logo_db = str_replace('/', '-', $logo);
       // $x_logo = explode('.', $logo);
       // $ekstensi_logo = strtolower(end($x_logo));
       // $ukuran_logo    = $_FILES['logo_mou']['size'];
       // $file_tmp_logo = $_FILES['logo_mou']['tmp_name'];

        if (in_array($ekstensi_draf, $ekstensi_diperbolehkan_draf) === true) {
       // if ((in_array($ekstensi_logo, $ekstensi_diperbolehkan_logo) === true) and (in_array($ekstensi_draf, $ekstensi_diperbolehkan_draf) === true)) {

            move_uploaded_file($file_tmp_draf, 'file_draf_mou/' . $nama_file_db);
            //move_uploaded_file($file_tmp_logo, 'logo_mou/' . $nama_logo_db);

            $simpan = mysqli_query($connect, "INSERT INTO tb_draf_mou (file_draf_mou_stmik,  id_proposal) VALUES ('$nama_file_db','$_GET[id]')");

            //$simpan = mysqli_query($connect, "INSERT INTO tb_draf_mou (file_draf_mou_stmik, logo_stmik, id_proposal) VALUES ('$nama_file_db','$nama_logo_db','$_GET[id]')");


            if (!$simpan) {
                echo '<div class="col-md-12">
                        <div class="alert alert-danger" role="alert">
                            <a href="index.php?admin=15&id=' . $_GET["id"] . '" class="close" data-dismiss="alert">
                            <span aria-hidden="true">&times;</span>
                            </a>
                            <strong>Proses Gagal!</strong> Terdapat kesalahan dalam pengiriman data.
                        </div>  
                    </div>';
            } else {
                echo '<div class="col-md-12">
                        <div class="alert alert-success" role="alert">
                        <a href="index.php?admin=15&id=' . $_GET["id"] . '" class="close" data-dismiss="alert">
                            <span aria-hidden="true">&times;</span>                    
                            </a>
                            <strong>Sukses!</strong> Draf berhasil di kirimkan <a href="index.php?admin=5" class="btn btn-danger btn-sm">Lihat Proposal</a>
                        </div>
                    </div>';
            }
        } else {
            echo '<div class="col-md-12">
                    <div class="alert alert-danger" role="alert">
                        <a href="index.php?admin=15&id=' . $_GET["id"] . '" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                        </a>
                        <strong>Proses Gagal!</strong>  Draf harus format. pdf
                    </div>  
                </div>';
        }
    }
}
?>

<?php
$tampilkan = mysqli_query($connect, "SELECT * FROM tb_draf_mou where  id_proposal ='$_GET[id]'");
$jumlah = mysqli_num_rows($tampilkan);

foreach ($tampilkan as $data) {

    $filekan = $data['file_draf_mou_stmik'];
    //$logokan = $data['logo_stmik'];
}
?>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">KIRIM DRAF MOU KE MITRA</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>
                <form action="" method="POST" enctype="multipart/form-data">
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">File Draf MOU</label>
                            <div class="col-md-10">
                                <input type="file" class="fileinput btn-primary" name="file_draf_mou" id="file_draf_mou" title="Browse file">
                                <?php if ($jumlah == 1) {
                                    echo '<a target="_blank" href="file_draf_mou/'.$filekan.'">LIHAT FILE</a>';
                                } else {
                                    echo '';
                                }
                                ?>
                                <input type="hidden" class="form-control" name="" id="" style="width: 200px; color: red;" value="<?php echo @$filekan ?>" readonly>
                                <span class="help-block">File berbentuk .PDF</span>
                                <br>
                                <br>
                            </div>
                        </div>

                        <!--<div class="form-group">
                            <label class="col-md-3 control-label">Kirim Logo</label>
                            <div class="col-md-10">
                                <input type="file" class="fileinput btn-primary" name="logo_mou" id="logo_mou" title="Browse file">
                                <?php if ($jumlah == 1) {
                                    echo '<a target="_blank" href="logo_mou/'.$logokan.'">LIHAT LOGO</a>';
                                } else {
                                    echo '';
                                }
                                ?>
                                <input type="hidden" class="form-control" name="" id="" style="width: 200px; color: red;" value="<?php echo @$logokan ?>" readonly>
                                <span class="help-block">File berbentuk .jpg, png, jpeg</span>
                            </div>
                        </div>-->

                    </div>
                    <div class="panel-footer">
                        <div class="form-group">
                            <button class="btn btn-primary pull-left" name="simpan">KIRIM KE MITRA<span class="fa fa-floppy-o fa-right"></span></button>
                            <!-- <a href="index.php?admin=5" class="btn btn-primary pull-right" name="simpan">Kembali <span class="fa fa fa-mail-reply-all"></span></a> -->
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>