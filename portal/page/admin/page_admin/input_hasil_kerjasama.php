<?php
if (isset($_POST['simpan'])) {
    $q = mysqli_query($connect, "SELECT * from tb_hasil_kerjasama where id_proposal ='$_GET[id]'");
    $row = mysqli_fetch_array($q);

    if (mysqli_num_rows($q) == 1) {
        $updatekan = mysqli_query($connect, "UPDATE tb_hasil_kerjasama SET  hasil_kerjasama='$_POST[hasil_mou]', kerjasama_selanjutnya='$_POST[mou_selanjutnya]' where id_proposal ='$_GET[id]'");
        if ($connect) {
            echo '<div class="col-md-12">
            <div class="alert alert-success" role="alert">
            <a href="index.php?admin=9" class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>                    
                </a>
                <strong>Sukses!</strong> Hasil kerjasama atau mou sudah diupdate
            </div>
        </div>';
        } else {
            echo '<div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <a href="index.php?admin=9" class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
                </a>
                <strong>Proses Gagal!</strong> Hasil kerjasama atau mou Tidak Terupdate.
            </div>  
        </div>';
        }
    } else {
        $simpan = mysqli_query($connect, "INSERT INTO tb_hasil_kerjasama (id_proposal,hasil_kerjasama,kerjasama_selanjutnya) VALUES ('$_GET[id]','$_POST[hasil_mou]','$_POST[mou_selanjutnya]')");

        if (!$simpan) {
            echo '<div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <a href="index.php?admin=9" class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
                </a>
                <strong>Proses Gagal!</strong> Hasil kerjasama mou Tidak di simpan.
            </div>  
        </div>';
        } else {
            echo '<div class="col-md-12">
        <div class="alert alert-success" role="alert">
        <a href="index.php?admin=9" class="close" data-dismiss="alert">
            <span aria-hidden="true">&times;</span>                    
            </a>
            <strong>Sukses!</strong> Hasil kerjasama mou sudah disimpan <a href="index.php?admin=9" class="btn btn-danger btn-sm">Lihat Proposal</a>
        </div>
    </div>';
        }
    }
}


?>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">INPUT HASIL KERJASAMA</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>

                <?php
                $no = 1;

                $tampilkan = mysqli_query($connect, "SELECT * FROM tb_hasil_kerjasama where id_proposal='$_GET[id]'");

                foreach ($tampilkan as $data) {
                    $hasil = $data['hasil_kerjasama'];
                    $hasil2 = $data['kerjasama_selanjutnya'];
                }
                ?>

                <form action="" method="POST">
                    <div class="col-md-12">

                        <div class="block">
                            <h4> 1. Manfaat yang Telah Diperoleh </h4>
                            <br>
                            <textarea class="summernote" name="hasil_mou"><?php echo @$hasil; ?></textarea>
                        </div>

                        <div class="block">
                            <h4> 2. Perencanaan Kerjaasama Berikutnya</h4>
                            <br>
                            <textarea class="summernote" name="mou_selanjutnya"><?php echo @$hasil2; ?></textarea>
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-default">Clear Form</button>
                            <button class="btn btn-primary pull-right" name="simpan" type="submit">Simpan</button>
                        </div>

                    </div>
                </form>

            </div>
        </div>
    </div>