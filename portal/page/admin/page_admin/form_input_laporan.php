<?php
if (isset($_POST['simpan'])) {
    $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
    $kode_random = substr(str_shuffle($permitted_chars), 0, 5);
    $ekstensi_diperbolehkan_draf = array('pdf');
    $nama_draf = $_FILES['file_draf_mou']['name'];
    $draf = $kode_random . '-' . date('Y-m-d') . '-' . $nama_draf;
    $nama_file_db = str_replace('/', '-', $draf);
    $x_draf = explode('.', $draf);
    $ekstensi_draf = strtolower(end($x_draf));
    $ukuran_draf    = $_FILES['file_draf_mou']['size'];
    $file_tmp_draf = $_FILES['file_draf_mou']['tmp_name'];

    if (in_array($ekstensi_draf, $ekstensi_diperbolehkan_draf) === true) {
        move_uploaded_file($file_tmp_draf, 'file_laporan/' . $nama_file_db);
        $updatekan = mysqli_query($connect, "UPDATE tb_proposal_mitra SET  laporan_kerja_sama='$nama_file_db' where id_proposal ='$_GET[id]'");

        if (!$updatekan) {
            echo '<div class="col-md-12">
                    <div class="alert alert-danger" role="alert">
                        <a href="index.php?admin=21&id=' . $_GET["id"] . '" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                        </a>
                        <strong>Proses Gagal!</strong> Terdapat kesalahan dalam pengiriman data.
                    </div>  
                </div>';
        } else {
            echo '<div class="col-md-12">
                    <div class="alert alert-success" role="alert">
                    <a href="index.php?admin=21&id=' . $_GET["id"] . '" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>                    
                        </a>
                        <strong>Sukses!</strong> Laporan berhasil di inputkan <a href="index.php?admin=9" class="btn btn-danger btn-sm">Lihat Laporan</a>
                    </div>
                </div>';
        }
    } else {
        echo '<div class="col-md-12">
                <div class="alert alert-danger" role="alert">
                    <a href="index.php?admin=21&id=' . $_GET["id"] . '" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    </a>
                    <strong>Proses Gagal!</strong> harus format. pdf
                </div>  
            </div>';
    }
}
?>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">INPUT LAPORAN KERJASAMA</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>
                <form action="" method="POST" enctype="multipart/form-data">
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Input Laporan</label>
                            <div class="col-md-10">
                                <input type="file" class="fileinput btn-primary" name="file_draf_mou" id="file_draf_mou" title="Browse file">
                                <span class="help-block">File berbentuk .pdf</span>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group">
                                <button class="btn btn-primary pull-left" name="simpan">Simpan</button>
                                <!-- <a href="index.php?admin=5" class="btn btn-primary pull-right" name="simpan">Kembali <span class="fa fa fa-mail-reply-all"></span></a> -->
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>