<?php
include "../../config/koneksi.php";
$nama_admin = $_SESSION['nama_admin'];
$id_admin = $_SESSION['id_admin'];
$status = $_SESSION['status'];
if ($_SESSION['id_admin'] == null || $_SESSION['id_admin'] == 0) {
    header("location:login/error_admin.php");
} else {
}
?>

<?php
if (@$_GET['id'] == 'In_proses') {
    echo '<div class="col-md-12">
            <div class="alert alert-success" role="alert">
            <a href="index.php?admin=2" class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
                
                </a>
                <strong>Sukses!</strong> Proposal kerjasama dalam proses
        </div>';
} elseif (@$_GET['id'] == 'Di_setujui') {
    echo '<div class="col-md-12">
            <div class="alert alert-success" role="alert">
            <a href="index.php?admin=2" class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
                
                </a>
                <strong>Sukses!</strong> Proposal kerjasama Berhasil disetujui <a href="index.php?admin=5" class="btn btn-danger btn-sm">Lihat Proposal</a>
        </div>';
} elseif (@$_GET['id'] == 'Di_tolak') {
    echo '<div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <a href="index.php?admin=2" class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
                </a>
                <strong>Proses Berhasil!</strong> Proposal di tolak
            </div>  
        </div>';
} else {
    echo '';
}

?>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">PERMOHONAN KERJASAMA MASUK</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>Action</th>
                                <th>Nama Instansi</th>
                                <th>Nama Mitra</th>
                                <th>Jabatan</th>
                                <th>No telpon</th>
                                <th>Alamat</th>                                
                                <th>Tgl Pengajuan</th>
                                <th>Bidang Kerjasama</th>
                                <th>File Proposal</th>
                                <th>Surat Pengantar</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampilkan = mysqli_query($connect, "SELECT * FROM tb_proposal_mitra where status_proposal!='Pending' and status_proposal!='Di Setujui'  ORDER BY id_proposal DESC");
                            foreach ($tampilkan as $data) {

                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td>
                                        <?php if ($status == 'admin') { ?>
                                            <div class="btn-group">
                                                <a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false">Action <span class="caret"></span></a>
                                                <ul class="dropdown-menu" role="menu">
                                                    <?php if($data['status_proposal'] !='Di Tolak'){?>
                                                    <!--<li><a href="index.php?admin=3&id=<?php echo $data['id_proposal']; ?>">View</a></li>-->
                                                    <li><a href="index.php?admin=4&id=<?php echo $data['id_proposal']; ?>&Proses=Di_proses">Di Proses</a></li>
                                                    <li><a href="index.php?admin=4&id=<?php echo $data['id_proposal']; ?>&Proses=Di_setujui">Di Setujui</a></li>
                                                    <li><a href="index.php?admin=4&id=<?php echo $data['id_proposal']; ?>&Proses=Di_tolak">Tolak</a></li>
                                                    <?php }else{?>
                                                   <!-- <li><a href="index.php?admin=3&id=<?php echo $data['id_proposal']; ?>">View</a></li>-->
                                                    <li><a href="index.php?admin=20&id=<?php echo $data['id_proposal']; ?>&Proses=Di_tolak">Delete</a></li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        <?php } else { ?>
                                            <a title="View Propoasal" href="index.php?admin=3&id=<?php echo $data['id_proposal']; ?>" class="btn-info btn-rounded btn-sm"><span class="glyphicon glyphicon-eye-open"></span></a>
                                        <?php } ?>
                                    </td>
                                    <td><?php echo $data['nama_instansi']; ?></td>
                                    <td><?php echo $data['nama_mitra']; ?></td>
                                    <td><?php echo $data['jabatan']; ?></td>
                                     <td><?php echo $data['no_telfon']; ?></td>
                                      <td><?php echo $data['alamat_instansi']; ?></td>                                    
                                    <td><?php echo $data['tgl_pengajuan']; ?></td>
                                    <td><?php echo $data['bidang_kerjasan']; ?></td>
                                    <td>
                                        <a title="View Data Proposal Pengajuan Kerjasama" target="_blank" href="../mitra/file/<?php echo $data['file_proposal']; ?>" class="btn btn-info"><i class="fa fa-folder-open"></i></a>
                                    </td>
                                    <td>
                                        <a title="View Surat Pengantar" target="_blank" href="../mitra/surat_pengantar/<?php echo $data['surat_pengantar']; ?>" class="btn btn-info"><i class="fa fa-folder-open"></i></a>
                                    </td>
                                    <td><?php if ($data['status_proposal'] == 'Terkirim') {
                                            echo 'Belum Proses';
                                        } else {
                                            echo $data['status_proposal'];
                                        }; ?></td>

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <br>
            <br>
            <!-- <i>Info.</i><br>
            <i>1. Info akan diberikan kembali dalam waktu 2 hari, untuk status proposal di proses..</i><br>
            <i>2. Admin kami akan menghubungi anda jika proposal kerjasama di sepakati. dengan jangka waktu 2 hari.</i><br>
            <i>3. Pending = anda belum melakukan pengiriman proposal dengan mencentang (data sudah lengkap).</i><br>
            <i>4. Terkirim = anda sudah melakukan pengiriman proposal.</i><br>
            <i>5. Di Tolak = Proposal andan di tolak (dengan alasan tertentu).</i> -->
        </div>
    </div>
</div>