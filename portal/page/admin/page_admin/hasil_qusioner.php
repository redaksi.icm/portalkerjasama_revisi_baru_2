<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">HASIL QUSIONER</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <?php
                    $no = 1;
                    $tampilkan = mysqli_query($connect, "SELECT * FROM tb_proposal_mitra where id_proposal = '$_GET[id]'");
                    foreach ($tampilkan as $data) {

                    ?>
                        <div class="col-md-4">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>Nama</td>
                                        <td>:</td>
                                        <td><?php echo $data['nama_mitra']; ?></td>

                                    </tr>
                                    <tr>
                                        <td>Instansi</td>
                                        <td>:</td>
                                        <td> <?php echo $data['nama_instansi']; ?></td>

                                    </tr>
                                    <tr>
                                        <td>Jabatan</td>
                                        <td>:</td>
                                        <td> <?php echo $data['jabatan']; ?></td>

                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td>:</td>
                                        <td> <?php echo $data['alamat_instansi']; ?></td>

                                    </tr>
                                    <tr>
                                        <td>Telpon</td>
                                        <td>:</td>
                                        <td> <?php echo $data['no_telfon']; ?></td>

                                    </tr>
                                    <tr>
                                        <td>Bidang Kerjasama</td>
                                        <td>:</td>
                                        <td> <?php echo $data['bidang_kerjasan']; ?></td>

                                    </tr>
                                    <tr>
                                        <td>Kesimpulan Qusioner</td>
                                        <td>:</td>
                                        <?php
                                        $query1 = mysqli_fetch_array(mysqli_query($connect, "SELECT count(*)  as total_1 FROM tb_jawaban where jawaban = 1 and id_proposal = '$_GET[id]'"));
                                        $query2 = mysqli_fetch_array(mysqli_query($connect, "SELECT count(*)  as total_2 FROM tb_jawaban where jawaban = 2 and id_proposal = '$_GET[id]'"));
                                        $query3 = mysqli_fetch_array(mysqli_query($connect, "SELECT count(*)  as total_3 FROM tb_jawaban where jawaban = 3 and id_proposal = '$_GET[id]'"));
                                        // batas

                                        $query4 = mysqli_fetch_array(mysqli_query($connect, "SELECT count(*)  as total_4 FROM tb_jawaban where jawaban = 4 and id_proposal = '$_GET[id]'"));
                                        $query5 = mysqli_fetch_array(mysqli_query($connect, "SELECT count(*)  as total_5 FROM tb_jawaban where jawaban = 5 and id_proposal = '$_GET[id]'"));
                                        ?>
                                        <td><?php
                                            $total1 = $query1['total_1'] + $query2['total_2'] + $query3['total_3'];
                                            $total2 = $query4['total_4'] + $query5['total_5'];

                                            if ($total2 > $total1) {
                                                echo 'Puas';
                                            } else {
                                                echo 'Tidak Puas';
                                            }
                                            ?></td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    <?php } ?>
                    <div class="col-md-12">
                        Petunjuk pengisian <br>
                        <i>Berilah tanda √ pada koloom 1 s.d 5 sesuai penilaian Bapak / Ibu, dangan keterangan sebagai
                            berikut <br>
                            1 Kurang Sekali <br>
                            2 Kurang <br>
                            3 Sedang <br>
                            4 Baik <br>
                            5 Baik Sekali</i><br>
                        <br>
                        <br>
                        <br>
                    </div>

                    </p>

                    <div class="col-md-12">
                        <i>B. Penilaian Terhadap Kerjasama </i>
                        <p></p>
                        <?php
                        $query = mysqli_fetch_array(mysqli_query($connect, "SELECT count(id_pertanyaan) as total_new FROM tb_pertanyaan_kusioner"));
                        ?>
                        <i>
                            Jumlah pertanyaan: <?php echo "$query[total_new]"; ?>
                        </i>
                        <br>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kategori</th>
                                    <th>1</th>
                                    <th>2</th>
                                    <th>3</th>
                                    <th>4</th>
                                    <th>5</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                $tampilkan = mysqli_query($connect, "SELECT tb_pertanyaan_kusioner.*, tb_jawaban.* FROM tb_pertanyaan_kusioner INNER JOIN tb_jawaban on tb_pertanyaan_kusioner.id_pertanyaan = tb_jawaban.id_pertanyaan where tb_jawaban.id_proposal = '$_GET[id]'");
                                foreach ($tampilkan as $data) {

                                ?>
                                    <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $data['kategori_pertanyaan']; ?></td>
                                        <td>
                                            <?php if ($data['jawaban'] == 1) {
                                                echo '<span class="glyphicon glyphicon-ok"></span>';
                                            }
                                            ?>
                                        </td>
                                        <td> <?php if ($data['jawaban'] == 2) {
                                                    echo '<span class="glyphicon glyphicon-ok"></span>';
                                                }
                                                ?></td>
                                        <td> <?php if ($data['jawaban'] == 3) {
                                                    echo '<span class="glyphicon glyphicon-ok"></span>';
                                                }
                                                ?></td>
                                        <td> <?php if ($data['jawaban'] == 4) {
                                                    echo '<span class="glyphicon glyphicon-ok"></span>';
                                                }
                                                ?></td>
                                        <td> <?php if ($data['jawaban'] == 5) {
                                                    echo '<span class="glyphicon glyphicon-ok"></span>';
                                                }
                                                ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-md-12">
                        <i>C. Rencana tindak lanjut dan saran perbaikan</i>
                        <p></p>
                        <?php
                        $query = mysqli_fetch_array(mysqli_query($connect, "SELECT count(id_pertanyaan_saran) as total_new1 FROM tb_pertanyaan_saran"));
                        ?>
                        <i>
                            Jumlah pertanyaan: <?php echo "$query[total_new1]"; ?>
                        </i>

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Pertanyaan</th>
                                    <th>Saran perbaikan </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                $tampilkan = mysqli_query($connect, "SELECT tb_pertanyaan_saran.*, tb_jawaban_saran.* FROM tb_pertanyaan_saran INNER JOIN tb_jawaban_saran on tb_pertanyaan_saran.id_pertanyaan_saran = tb_jawaban_saran.id_pertanyaan_saran where tb_jawaban_saran.id_proposal = '$_GET[id]'");
                                foreach ($tampilkan as $data) {

                                ?>
                                    <tr>
                                        <th><?php echo $no++; ?></th>
                                        <th><?php echo $data['pertanyaan_saran']; ?></th>
                                        <th><?php echo $data['jawaban_saran']; ?></th>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <div class="panel-footer">
                             <a href="index.php?admin=9" class="btn btn-primary pull-left">Kembali <span class="fa fa fa-mail-reply-all"></span></a>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>