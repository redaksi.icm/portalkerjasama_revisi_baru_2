<?php
$no = 1;
$tampilkan = mysqli_query($connect, "SELECT * FROM tb_proposal_mitra where id_proposal = '$_GET[id]'");
foreach ($tampilkan as $data) {

?>
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">VIEW PROPOSAL KERJASAMA</h3>
                    </div>
                    <form action="" method="POST" enctype="multipart/form-data">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Nama Mitra</label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="nama_mitra" value="<?php echo $data['nama_mitra']; ?>">
                                            </div>
                                            <span class="help-block">.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Jabatan</label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="jabatan_mitra" value="<?php echo $data['jabatan']; ?>">
                                            </div>
                                            <span class="help-block">.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">No. telp/HP</label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="no_telfon" value="<?php echo $data['no_telfon']; ?>">
                                            </div>
                                            <span class="help-block">.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Nama Instansi/PTS/PTN</label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="nama_instansi" value="<?php echo $data['nama_instansi']; ?>">
                                            </div>
                                            <span class="help-block">.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Alamat Instansi/PTS/PTN</label>
                                        <div class="col-md-9 col-xs-12">
                                            <textarea class="form-control" rows="5" name="alamat_instansi"><?php echo $data['alamat_instansi']; ?></textarea>
                                            <span class="help-block">.</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Tanggal Pengajuan</label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" class="form-control datepicker" name="tgl_proposal" value="<?php echo $data['tgl_pengajuan']; ?>">
                                            </div>
                                            <span class="help-block">.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Bidang Kerjasama</label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="bidang" value="<?php echo $data['bidang_kerjasan']; ?>">
                                            </div>
                                            <span class="help-block">.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">File Proposal Kerjasama</label>
                                        <div class="col-md-9">
                                            <div class="col-md-4">
                                                <div class="widget" style="min-height: unset;">
                                                    <a class="widget-item-left" target="_blank" href="../mitra/file/<?php echo $data['file_proposal']; ?>">
                                                        <span class="fa fa-folder-open" style="color: cadetblue;"></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div class="col-md-9">
                                            <label class="check"><input type="checkbox" class="icheckbox" checked="checked" name="status_data" value="Terkirim" /> Data sudah lengkap.</label>
                                            <span class="help-block">Dengan mencentang, maka data tidak bisa di edit lagi.</span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <?php if (@$_GET['session'] == 'hidden') { ?>
                                <div class="form-group">
                                    <a href="index.php?admin=5" class="btn btn-primary pull-left" name="simpan">Kembali <span class="fa fa fa-mail-reply-all"></span></a>
                                </div>
                            <?php } elseif (@$_GET['session'] == 'pesan') { ?> 
                                <div class="form-group">
                                    <a href="index.php?admin=8" class="btn btn-primary pull-left" name="simpan">Kembali <span class="fa fa fa-mail-reply-all"></span></a>
                                </div> 
                            <?php } elseif(@$_GET['session'] == 'qusioner') { ?>
                                <div class="form-group">
                                    <a href="index.php?admin=9" class="btn btn-primary pull-left" name="simpan">Kembali <span class="fa fa fa-mail-reply-all"></span></a>
                                </div> 
                            <?php } else { ?>
                                <div class="form-group">
                                    <a href="index.php?admin=2" class="btn btn-primary pull-left" name="simpan">Kembali <span class="fa fa fa-mail-reply-all"></span></a>
                                </div>
                            <?php } ?>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>