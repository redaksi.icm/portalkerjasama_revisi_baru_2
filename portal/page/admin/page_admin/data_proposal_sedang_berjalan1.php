<?php
include "../../config/koneksi.php";
$nama_admin = $_SESSION['nama_admin'];
$id_admin = $_SESSION['id_admin'];
$status = $_SESSION['status'];
if ($_SESSION['id_admin'] == null || $_SESSION['id_admin'] == 0) {
    header("location:login/error_admin.php");
} else {
}
?>


<?php
if (@$_GET['session'] == 'Terkirim') {
    echo '<div class="col-md-12">
            <div class="alert alert-success" role="alert">
            <a href="index.php?admin=8" class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
                
                </a>
                <strong>Sukses!</strong> Qusioner Sudah Di Kirim!!
        </div>';
} elseif (@$_GET['session'] != 'Terkirim') {
    echo '';
}

?>

<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">KERJASAMA DALAM PELAKSANAAN</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>Action</th>
                                <th>File MoU</th>
                                <th>No Mou STMIK</th>
                                <th>No Mou Mitra</th>
                                <th>Nama Instansi
                                <th>Masa Berlaku(Tahun)</th>
                                <th>Nama Mitra</th>
                                <th>Jabatan</th>                               
                                <th>Tgl TTD</th>
                                <!-- <th>Bidang Kerjasama</th> -->
                                <!-- <th>File</th> -->
                                <!-- <th>Kategori</th> -->
                                <th>Status Qusioner</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampilkan = mysqli_query($connect, "SELECT tb_proposal_mitra.*, tb_file_mou.* FROM tb_proposal_mitra INNER JOIN tb_file_mou on tb_file_mou.id_proposal=tb_proposal_mitra.id_proposal where tb_proposal_mitra.status_proposal='Di Setujui' and tb_proposal_mitra.no_mou != '-'  ORDER BY tb_proposal_mitra.id_proposal DESC");
                            // $tampilkan = mysqli_query($connect, "SELECT * FROM tb_proposal_mitra where status_proposal='Di Setujui' and no_mou != '-'  ORDER BY id_proposal DESC");
                            foreach ($tampilkan as $data) {

                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td>
                                        <?php if ($status == 'admin') { ?>
                                            <div class="btn-group">
                                                <a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false">Action <span class="caret"></span></a>
                                                <ul class="dropdown-menu" role="menu">
                                                    <?php if ($data['no_mou'] != '-') { ?>
                                                  <!--  <?php if ($data['no_mou'] != '-' && $data['no_mou_mitra'] != '-') { ?>-->
                                                        <li><a href="index.php?admin=3&id=<?php echo $data['id_proposal']; ?>&session=pesan">View</a></li>
                                                        <li><a href="index.php?admin=7&id=<?php echo $data['id_proposal']; ?>">Edit Nomor MOU</a></li>
                                                        <?php if ($data['status_qusioner'] != 'Selesai') { ?>
                                                        <li><a href="index.php?admin=10&id=<?php echo $data['id_proposal']; ?>">Kirim Qusioner</a></li>
                                                        <?php } ?>
                                                    <?php } else { ?>
                                                        <li><a href="index.php?admin=3&id=<?php echo $data['id_proposal']; ?>&session=pesan">View</a></li>
                                                        <li><a href="index.php?admin=7&id=<?php echo $data['id_proposal']; ?>">Edit Nomor MOU</a></li>
                                                        
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        <?php } else { ?>

                                            <a title="View Propoasal" href="index.php?admin=3&id=<?php echo $data['id_proposal']; ?>&session=pesan" class="btn-info btn-rounded btn-sm"><span class="glyphicon glyphicon-eye-open"></span></a>
                                        <?php } ?>
                                         <?php } ?>
                                    </td>

                                    <!--<td><a href="file_mou/<?php echo $data['file_mou_stmik']; ?>" target="_blank"><?php echo $data['no_mou']; ?></a></td>
                                    <td><a href="file_mou/<?php echo $data['file_mou_stmik']; ?>" target="_blank"><?php echo $data['no_mou_mitra']; ?></a></td>-->
                                    <td>
                                        <a title="View file MoU" target="_blank" href="file_mou/<?php echo $data['file_mou_stmik']; ?>" class="btn btn-info"><i class="fa fa-folder-open"></i></a>
                                    </td>
                                    <td><?php echo $data['no_mou']; ?></td>
                                    <td><?php echo $data['no_mou_mitra']; ?></td>
                                    <td><?php echo $data['nama_instansi']; ?></td>
                                    <td><?php echo $data['masa_berlaku']; ?></td>                                    
                                    <td><?php echo $data['nama_mitra']; ?></td>
                                    <td><?php echo $data['jabatan']; ?></td>                                    
                                    <td><?php echo $data['tgl_mou']; ?></td>
                                    <!-- <td><?php echo $data['bidang_kerjasan']; ?></td> -->
                                    <!-- <td>
                                        <a title="View Data Proposal Pengajuan Kerjasama" target="_blank" href="../mitra/file/<?php echo $data['file_proposal']; ?>" class="btn btn-info"><i class="fa fa-folder-open"></i></a>
                                    </td> -->
                                    <!-- <td><?php echo $data['kategori_proposal']; ?></td> -->
                                    <td>
                                        <?php if ($data['status_qusioner'] == 'Terkirim') { ?>
                                            Terkirim
                                        <?php } elseif ($data['status_qusioner'] == 'Selesai') { ?>
                                            Sudah di isi
                                        <?php } else { ?>
                                            Belum di kirim
                                        <?php } ?>
                                    </td>

                                </tr>
                            <?php } ?>
                            
                        </tbody>
                    </table>
                </div>
            </div>
            <br>
            <br>
            <!-- <i>Info.</i><br>
            <i>1. Info akan diberikan kembali dalam waktu 2 hari, untuk status proposal di proses..</i><br>
            <i>2. Admin kami akan menghubungi anda jika proposal kerjasama di sepakati. dengan jangka waktu 2 hari.</i><br>
            <i>3. Pending = anda belum melakukan pengiriman proposal dengan mencentang (data sudah lengkap).</i><br>
            <i>4. Terkirim = anda sudah melakukan pengiriman proposal.</i><br>
            <i>5. Di Tolak = Proposal andan di tolak (dengan alasan tertentu).</i> -->
        </div>
    </div>
</div>