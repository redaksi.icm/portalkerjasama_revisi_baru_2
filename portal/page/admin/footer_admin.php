<audio id="audio-alert" src="../../desain/audio/alert.mp3" preload="auto"></audio>
<audio id="audio-fail" src="../../desain/audio/fail.mp3" preload="auto"></audio>
<script type="text/javascript" src="../../desain/js/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="../../desain/js/plugins/jquery/jquery-ui.min.js"></script>
<script type="text/javascript" src="../../desain/js/plugins/bootstrap/bootstrap.min.js"></script>
<script type='text/javascript' src='../../desain/js/plugins/icheck/icheck.min.js'></script>
<script type="text/javascript" src="../../desain/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>

<script type="text/javascript" src="../../desain/js/plugins/codemirror/codemirror.js"></script>
<script type='text/javascript' src="../../desain/js/plugins/codemirror/mode/htmlmixed/htmlmixed.js"></script>
<script type='text/javascript' src="../../desain/js/plugins/codemirror/mode/xml/xml.js"></script>
<script type='text/javascript' src="../../desain/js/plugins/codemirror/mode/javascript/javascript.js"></script>
<script type='text/javascript' src="../../desain/js/plugins/codemirror/mode/css/css.js"></script>
<script type='text/javascript' src="../../desain/js/plugins/codemirror/mode/clike/clike.js"></script>
<script type='text/javascript' src="../../desain/js/plugins/codemirror/mode/php/php.js"></script>

<script type="text/javascript" src="../../desain/js/plugins/summernote/summernote.js"></script>

<script type="text/javascript" src="../../desain/js/plugins/datatables/jquery.dataTables.min.js"></script>

<script type="text/javascript" src="../../desain/js/plugins/knob/jquery.knob.min.js"></script>

<script type="text/javascript" src="../../desain/js/plugins/scrolltotop/scrolltopcontrol.js"></script>
<script type="text/javascript" src="../../desain/js/plugins/morris/raphael-min.js"></script>
<script type="text/javascript" src="../../desain/js/plugins/morris/morris.min.js"></script>
<script type="text/javascript" src="../../desain/js/plugins/rickshaw/d3.v3.js"></script>
<script type="text/javascript" src="../../desain/js/plugins/rickshaw/rickshaw.min.js"></script>
<script type='text/javascript' src='../../desain/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'></script>
<script type='text/javascript' src='../../desain/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'></script>
<script type='text/javascript' src='../../desain/js/plugins/bootstrap/bootstrap-datepicker.js'></script>
<script type="text/javascript" src="../../desain/js/plugins/owl/owl.carousel.min.js"></script>
<script type="text/javascript" src="../../desain/js/plugins/moment.min.js"></script>
<script type="text/javascript" src="../../desain/js/plugins/daterangepicker/daterangepicker.js"></script>
<script type='text/javascript' src='../../desain/js/plugins/noty/jquery.noty.js'></script>
<script type='text/javascript' src='../../desain/js/plugins/noty/layouts/topCenter.js'></script>
<script type='text/javascript' src='../../desain/js/plugins/noty/layouts/topLeft.js'></script>
<script type='text/javascript' src='../../desain/js/plugins/noty/layouts/topRight.js'></script>
<script type='text/javascript' src='../../desain/js/plugins/noty/themes/default.js'></script>
<script type="text/javascript" src="../../desain/js/plugins/tagsinput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="../../desain/js/plugins/bootstrap/bootstrap-select.js"></script>
<script type="text/javascript" src="../../desain/js/plugins/bootstrap/bootstrap-file-input.js"></script>
<script type="text/javascript" src="../../desain/js/plugins/morris/raphael-min.js"></script>
<script type="text/javascript" src="../../desain/js/plugins/morris/morris.min.js"></script>
<script type="text/javascript" src="../../desain/js/demo_charts_morris.js"></script>

<script src="../../desain/fastclick/lib/fastclick.js"></script>

<script src="../../desain/Flot/jquery.flot.js"></script>
<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
<script src="../../desain/Flot/jquery.flot.resize.js"></script>
<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
<script src="../../desain/Flot/jquery.flot.pie.js"></script>
<!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
<script src="../../desain/Flot/jquery.flot.categories.js"></script>
<script>
    var editor = CodeMirror.fromTextArea(document.getElementById("codeEditor"), {
        lineNumbers: true,
        matchBrackets: true,
        mode: "application/x-httpd-php",
        indentUnit: 4,
        indentWithTabs: true,
        enterMode: "keep",
        tabMode: "shift"
    });
    editor.setSize('100%', '420px');
</script>




<script type="text/javascript">
    function notyConfirm() {
        noty({
            text: 'Do you want to continue?',
            layout: 'topRight',
            buttons: [{
                    addClass: 'btn btn-success btn-clean',
                    text: 'Ok',
                    onClick: function($noty) {
                        $noty.close();
                        noty({
                            text: 'You clicked "Ok" button',
                            layout: 'topRight',
                            type: 'success'
                        });
                    }
                },
                {
                    addClass: 'btn btn-danger btn-clean',
                    text: 'Cancel',
                    onClick: function($noty) {
                        $noty.close();
                        noty({
                            text: 'You clicked "Cancel" button',
                            layout: 'topRight',
                            type: 'error'
                        });
                    }
                }
            ]
        })
    }
</script>
<!-- END PAGE PLUGINS -->

<!-- START TEMPLATE -->
<!-- <script type="text/javascript" src="../../desain/js/settings.js"></script> -->
<script type="text/javascript" src="../../desain/js/plugins.js"></script>
<script type="text/javascript" src="../../desain/js/actions.js"></script>

<!-- END TEMPLATE -->
<!-- END SCRIPTS -->
<?php
$tampilkan = mysqli_query($connect, "SELECT
CASE bidang_kerjasan
WHEN 'bidang pendidikan dan pengajaran' THEN '1'   
WHEN 'bidang penelitian dan pengembangan ilmu' THEN '2'   
WHEN 'bidang pengabdian kepada masyarakat' THEN '3'   
WHEN 'bidang kegiatan lainnya' THEN '4'   
END as bidang,
count(bidang_kerjasan) as total
FROM tb_proposal_mitra group by bidang_kerjasan");
?>
<script>
    var bar_data = {
        data: [
            <?php foreach ($tampilkan as $data) { ?>['<?php echo $data['bidang']; ?>', <?php echo $data['total']; ?>],
            <?php  } ?>
        ],
        color: '#ff1010'
    }
    $.plot('#bar-chart', [bar_data], {
        grid: {
            borderWidth: 1,
            borderColor: '#f3f3f3',
            tickColor: '#f3f3f3'
        },
        series: {
            bars: {
                show: true,
                barWidth: 0.5,
                align: 'center'
            }
        },
        xaxis: {
            mode: 'categories',
            tickLength: 0
        }
    });

    // batas
    <?php
    $query1 = mysqli_fetch_array(mysqli_query($connect, "SELECT count(*)  as total_1 FROM tb_jawaban where jawaban = 1"));
    $query2 = mysqli_fetch_array(mysqli_query($connect, "SELECT count(*)  as total_2 FROM tb_jawaban where jawaban = 2"));
    $query3 = mysqli_fetch_array(mysqli_query($connect, "SELECT count(*)  as total_3 FROM tb_jawaban where jawaban = 3"));
    // batas
    $query4 = mysqli_fetch_array(mysqli_query($connect, "SELECT count(*)  as total_4 FROM tb_jawaban where jawaban = 4"));
    $query5 = mysqli_fetch_array(mysqli_query($connect, "SELECT count(*)  as total_5 FROM tb_jawaban where jawaban = 5"));
    ?>

    var donutData = [{
            label: '',
            data: '<?php echo $query1['total_1'] + $query2['total_2'] + $query3['total_3'];?>',
            color: '#ff1010'
        },

        {
            label: '',
            data: '<?php echo $query4['total_4'] + $query5['total_5'];?>',
            color: '#f39c12'
        },
    ]
    $.plot('#donut-chart', donutData, {
        series: {
            pie: {
                show: true,
                radius: 1,
                innerRadius: 0.5,
                label: {
                    show: true,
                    radius: 2 / 3,
                    formatter: labelFormatter,
                    threshold: 0.1
                }

            }
        },
        legend: {
            show: false
        }
    })

    function labelFormatter(label, series) {
        return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">' +
            label +
            '<br>' +
            Math.round(series.percent) + '%</div>'
    }
</script>


</body>

</html>