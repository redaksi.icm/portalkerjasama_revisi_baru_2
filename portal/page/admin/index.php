<?php
include "../../config/koneksi.php";
session_start();
$nama_admin = $_SESSION['nama_admin'];
$id_admin = $_SESSION['id_admin'];
$status = $_SESSION['status'];
if ($_SESSION['id_admin'] == null || $_SESSION['id_admin'] == 0) {
    header("location:login/error_admin.php");
} else {
}
?>
<?php include 'head_admin.php'; ?>
<!-- batas -->
<div class="page-container">
    <?php include 'sidebar_admin.php'; ?>
    <!-- loading -->
    <?php include 'login/loading_admin.php';?>
</div>
</div>

<!-- END PAGE CONTAINER -->
<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
            <div class="mb-content">
                <p>Apa kamu yakin untuk log out?</p>
                <p>klik No untuk batal.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <a href="login/logout_admin.php" class="btn btn-success btn-lg">Yes</a>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- batas -->
<?php include 'footer_admin.php'; ?>