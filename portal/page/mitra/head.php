<!DOCTYPE html>
<html lang="en" class="body-full-height">

<head>
    <!-- META SECTION -->
    <title>PORTAL - KERJASAMA STMIK IP</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="../../desain/css/theme-default.css" />
    <style type="text/css">
        .scroll1 {
            width: 100%;
            height: 100%;
            margin: 30px 30px 30px 0px;
            /* padding: 0px 20px 20px 0px; */
            overflow-y: auto;
            overflow-x: scroll;
        }
    </style>