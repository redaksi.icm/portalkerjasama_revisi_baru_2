<?php
include "../../config/koneksi.php";

if (isset($_GET["halaman"])) {
  $p = $_GET["halaman"];
} else {
  $p = 1;
}
switch ($p) {

  case "1":
    require("tampilan/beranda.php");
    break;
  case "2":
    require("tampilan/input_kerjasama.php");
    break;

  case "3":
    require("tampilan/tampilan_porosal_kerjasam.php");
    break;

  case "4":
    require("tampilan/view_proposal_kerjasama.php");
    break;

  case "5":
    require("tampilan/edit_proposal.php");
    break;

  case "6":
    require("tampilan/hapus_proposal.php");
    break;

  case "7":
    require("tampilan/catatan_mitra.php");
    break;

  case "8":
    require("tampilan/pelaksanaan_proposal.php");
    break;

  case "9":
    require("tampilan/input_mou_mitra.php");
    break;

  case "10":
    require("tampilan/hasil_dan_qusioner.php");
    break;

  case "11":
    require("tampilan/pertanyaan_qusioner.php");
    break;

  case "12":
    require("tampilan/qusioner_selesai.php");
    break;

  case "13":
    require("tampilan/hasil_kerjasama.php");
    break;

  case "14":
    require("tampilan/kirim_draf_mou_mitra.php");
    break;
}
