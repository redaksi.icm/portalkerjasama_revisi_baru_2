<?php
include "../../config/koneksi.php";
$nama_user = $_SESSION['nama_user'];
$id_user = $_SESSION['id_user'];
if ($_SESSION['id_user'] == null || $_SESSION['id_user'] == 0) {
    header("location:login/error.php");
} else {
}
?>
<?php
if (isset($_POST['simpan'])) {
    $nama_mitra = $_POST['nama_mitra'];
    $jabatan_mitra = $_POST['jabatan_mitra'];
    $no_telfon = $_POST['no_telfon'];
    $nama_instansi = $_POST['nama_instansi'];
    $alamat_instansi = $_POST['alamat_instansi'];
    $tgl_proposal = $_POST['tgl_proposal'];
    $bidang = $_POST['bidang'];
    @$status = $_POST['status_data'];
    $file_sekarang = $_POST['file_sekarang'];
    $file_update = $_FILES['file_proposal']['name'];

    if (!empty($file_update)) {

        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        $kode_random = substr(str_shuffle($permitted_chars), 0, 5);

        $ekstensi_diperbolehkan = array('pdf', 'pdf');
        $nama1 = $_FILES['file_proposal']['name'];
        $nama = $kode_random . '-' . $tgl_proposal . '-' . $nama1;
        $x = explode('.', $nama);
        $ekstensi = strtolower(end($x));
        $ukuran    = $_FILES['file_proposal']['size'];
        $file_tmp = $_FILES['file_proposal']['tmp_name'];
        move_uploaded_file($file_tmp, 'file/' . $nama);
    } else {
        $ekstensi_diperbolehkan = array('pdf', 'pdf');
        $nama = $file_sekarang;
        $x = explode('.', $nama);
        $ekstensi = strtolower(end($x));
    }
    // batas
    if ($status != 'Terkirim') {
        $status_data = 'Pending';
    } else {
        $status_data = 'Terkirim';
    }


    if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {

        $updatekan = mysqli_query($connect, "UPDATE tb_proposal_mitra SET  nama_mitra='$nama_mitra', jabatan='$jabatan_mitra', no_telfon='$no_telfon', nama_instansi='$nama_instansi', alamat_instansi='$alamat_instansi', tgl_pengajuan='$tgl_proposal', bidang_kerjasan='$bidang', file_proposal='$nama', status_proposal='$status_data', kategori_proposal='Dalam Negeri' where id_proposal = '$_GET[id]'");

        if (!$updatekan) {
            echo '<div class="col-md-12">
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>Proses Gagal!</strong> Terdapat kesalahan dalam pengiriman data.
                </div>  
            </div>';
        } else {
            if ($status_data == 'Pending') {
                echo '<div class="col-md-12">
                <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    
                    </button>
                    <strong>Sukses!</strong> Data tersimpan dan tidak terkirim, karna data masih kurang dan belum siap untuk di ajukan <a href="index.php?halaman=3" class="btn btn-danger btn-sm">Lihat Status Proposal</a>
                </div>
            </div>';
            } else {
                echo '<div class="col-md-12">
                <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    
                    </button>
                    <strong>Sukses!</strong> Data berhasil di kirim, kami akan menghubungi anda secepatnya., dan memproses data. kami senang ada sudah menjadi mitra kami!! <a href="index.php?halaman=3" class="btn btn-danger btn-sm">Lihat Status Proposal</a>
                </div>
            </div>';
            }
        }
    } else {
        echo '<div class="col-md-12">
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert">
            <span aria-hidden="true">&times;</span>
            </button>
            <strong>Proses Gagal!</strong> File harus berjenis .PDF
        </div>  
    </div>';
    }
} else {
}


?>

<?php
$no = 1;
$tampilkan = mysqli_query($connect, "SELECT * FROM tb_proposal_mitra where id_proposal = '$_GET[id]'");
foreach ($tampilkan as $data) {
?>
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">EDIT PROPOSAL KERJASAMA</h3><br><br><br>
                         <font color="red">Wajib isi tanda *</font>
                    </div>
                    <form action="" method="POST" enctype="multipart/form-data">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><font color="red">*</font>Nama Mitra</label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="nama_mitra" value="<?php echo $data['nama_mitra']; ?>" required="required">
                                            </div>
                                            <span class="help-block">.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><font color="red">*</font>Jabatan</label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="jabatan_mitra" value="<?php echo $data['jabatan']; ?>">
                                            </div>
                                            <span class="help-block">.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><font color="red">*</font>No. telp/HP</label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="no_telfon" value="<?php echo $data['no_telfon']; ?>">
                                            </div>
                                            <span class="help-block">.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><font color="red">*</font>Nama Instansi/PTS/PTN</label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="nama_instansi" value="<?php echo $data['nama_instansi']; ?>">
                                            </div>
                                            <span class="help-block">.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><font color="red">*</font>Alamat Instansi/PTS/PTN</label>
                                        <div class="col-md-9 col-xs-12">
                                            <textarea class="form-control" rows="5" name="alamat_instansi"><?php echo $data['alamat_instansi']; ?></textarea>
                                            <span class="help-block">.</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><font color="red">*</font>Tanggal Pengajuan</label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" class="form-control datepicker" name="tgl_proposal" value="<?php echo $data['tgl_pengajuan']; ?>">
                                            </div>
                                            <span class="help-block">.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <!-- <label class="col-md-3 control-label">Bidang Kerjasama</label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="bidang" value="<?php echo $data['bidang_kerjasan']; ?>">
                                            </div>
                                            <span class="help-block">.</span>
                                        </div> -->
                                        <label class="col-md-3 control-label"><font color="red">*</font>Bidang Kerjasama</label>
                                        <div class="col-md-9">
                                            <select class="" style="width: 375px;height: 30px;" name="bidang">
                                                <option value="<?php echo $data['bidang_kerjasan']; ?>"><?php echo $data['bidang_kerjasan']; ?></option>
                                                <option value="bidang pendidikan dan pengajaran">bidang pendidikan dan pengajaran</option>
                                                <option value="bidang penelitian dan pengembangan ilmu">bidang penelitian dan pengembangan ilmu</option>
                                                <option value="bidang pengabdian kepada masyarakat">bidang pengabdian kepada masyarakat</option>
                                                <option value="bidang kegiatan lainnya">bidang kegiatan lainnya</option>
                                                <!-- <option value="Pertukaran budaya yang dilakukan oleh dosen dan mahasiswa">Pertukaran budaya yang dilakukan oleh dosen dan mahasiswa</option>
                                                <option value="Pemanfaatan sumberdaya dalam pelaksanaan pendidikan dan kegiatan akademik lainnya yaitu penitian dan pengabdian masyarakat">Pemanfaatan sumberdaya dalam pelaksanaan pendidikan dan
                                                    kegiatan akademik lainnya yaitu penitian dan pengabdian masyarakat</option>
                                                <option value="Penerbitan bersama karya ilmiah">Penerbitan bersama karya ilmiah</option>
                                                <option value="Penyelenggaraan bersama kegiatan ilmiah, seminar dan kegiatan lainnya">Penyelenggaraan bersama kegiatan ilmiah, seminar dan kegiatan lainnya</option>
                                                <option value="Kerjasama lainnya">Kerjasama lainnya</option> -->
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <br>
                                        <br><br><br><br><br><br><br>
                                        <label class="col-md-3 control-label">Surat Pengntar</label>
                                       <div class="col-md-9">
                                            <?php if (!empty($data['surat_pengantar'])) { ?>
                                                <i>Name file: <?php echo $data['surat_pengantar']; ?></i>
                                                <input type="hidden" value="<?php echo $data['surat_pengantar']; ?>" name="file_sekarang">
                                                <div class="col-md-4">
                                                    <div class="widget" style="min-height: unset;">
                                                        <a class="widget-item-left" target="_blank" href="../mitra/surat_pengantar/<?php echo $data['surat_pengantar']; ?>">
                                                            <span class="fa fa-folder-open" style="color: cadetblue;"></span>
                                                        </a>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                File proposal kosong
                                            <?php } ?>-->

                                            <div class="col-md-9">
                                                <input type="file" class="fileinput btn-primary" name="surat_pengantar" id="surat_pengantar" title="Browse file">
                                                <span class="help-block">File berbentuk .PDF</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <br>
                                        <br><br><br><br><br><br><br>
                                        <label class="col-md-3 control-label">File Proposal Kerjasama</label>
                                        <div class="col-md-9">
                                            <?php if (!empty($data['file_proposal'])) { ?>
                                                <i>Name file: <?php echo $data['file_proposal']; ?></i>
                                                <input type="hidden" value="<?php echo $data['file_proposal']; ?>" name="file_sekarang">
                                                <div class="col-md-4">
                                                    <div class="widget" style="min-height: unset;">
                                                        <a class="widget-item-left" target="_blank" href="../mitra/file/<?php echo $data['file_proposal']; ?>">
                                                            <span class="fa fa-folder-open" style="color: cadetblue;"></span>
                                                        </a>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                File proposal kosong
                                            <?php } ?>

                                            <div class="col-md-9">
                                                <input type="file" class="fileinput btn-primary" name="file_proposal" id="file_proposal" title="Browse file">
                                                <span class="help-block">File berbentuk .PDF</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div class="col-md-9">
                                            <label class="check"><input type="checkbox" class="icheckbox" <?php if ($data['status_proposal'] == 'Pending') {
                                                  echo '';
                                                   } else {
                                                     echo 'checked';
                                                       } ?> name="status_data" value="Terkirim" /> Centang data sudah lengkap.</label>
                                            <span class="help-block">Dengan mencentang, maka data tidak bisa di edit lagi dan data akan dikirim.</span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group">
                                <a href="index.php?halaman=3" class="btn btn-primary pull-left">Kembali <span class="fa fa fa-mail-reply-all"></span></a>
                                <button class="btn btn-primary pull-right" name="simpan">Edit/ kirim ulang Permohonan<span class="fa fa-floppy-o fa-right"></span></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>