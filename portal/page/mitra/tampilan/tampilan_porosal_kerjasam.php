<?php
include "../../config/koneksi.php";
$nama_user = $_SESSION['nama_user'];
$id_user = $_SESSION['id_user'];
if ($_SESSION['id_user'] == null || $_SESSION['id_user'] == 0) {
    header("location:login/error.php");
} else {
}
?>
<?php
if (@$_GET['id'] == 'delete') {
    echo '<div class="col-md-12">
    <div class="alert alert-success" role="alert">
    <a href="index.php?halaman=3" class="close" data-dismiss="alert">
        <span aria-hidden="true">&times;</span>
        
        </a>
        <strong>Sukses!</strong> Data berhasil di hapus
</div>';
} elseif (@$_GET['id'] == 'gagal') {
    echo '<div class="col-md-12">
    <div class="alert alert-danger" role="alert">
        <a href="index.php?halaman=3" class="close" data-dismiss="alert">
        <span aria-hidden="true">&times;</span>
        </a>
        <strong>Proses Gagal!</strong> Data gagal di hapus.
    </div>  
</div>';
} else {
    echo '';
}

?>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">DATA PERMOHONAN KERJASAMA</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>Draf MoU STMIK</th>
                                <th>Nama Instansi</th>
                                <th>Nama Mitra</th>
                                <th>Jabatan</th>
                                <th>No telpon</th>
                                <th>Alamat</th>                                
                                <th>Tgl Pengajuan</th>
                                <th>Bidang Kerjasama</th>
                                <th>File Proposal</th>
                                <th>Surat Pengntar</th>
                                <th>Status</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            //$tampilkan = mysqli_query($connect, "SELECT tb_proposal_mitra.*, tb_draf_mou.file_draf_mou_stmik, tb_draf_mou.logo_stmik FROM tb_proposal_mitra LEFT JOIN tb_draf_mou on tb_draf_mou.id_proposal=tb_proposal_mitra.id_proposal where tb_proposal_mitra.id_user='$id_user' and tb_proposal_mitra.no_mou = '-' ORDER BY tb_proposal_mitra.id_proposal DESC");
                            $tampilkan = mysqli_query($connect, "SELECT tb_proposal_mitra.*, tb_draf_mou.file_draf_mou_stmik FROM tb_proposal_mitra LEFT JOIN tb_draf_mou on tb_draf_mou.id_proposal=tb_proposal_mitra.id_proposal where tb_proposal_mitra.id_user='$id_user' and tb_proposal_mitra.no_mou = '-' ORDER BY tb_proposal_mitra.id_proposal DESC");
                            foreach ($tampilkan as $data) {
                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td>
                                        <?php if ($data['status_proposal'] == 'Terkirim') {
                                            echo 'Draf belum dikirim';
                                            //echo '<a title="View Propoasal" href="index.php?halaman=4&id=' . $data['id_proposal'] . '" class="btn-info btn-rounded btn-sm"><span class="glyphicon glyphicon-eye-open"></span></a>';
                                        } elseif ($data['status_proposal'] == 'Di Setujui') {

                                        
                                             echo '<a  class="btn btn-info" title="View Draf MoU STMIK" target="_blank" href="../../page/admin/file_draf_mou/' . $data['file_draf_mou_stmik'] . '"><i class="fa fa-folder-open"></i></a>';

                                               // echo '<div class="btn-group">
                                                 //   <a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false">Draf Mou<span class="caret"></span></a>
                                                  //  <ul class="dropdown-menu" role="menu">';
                                                   // if (strlen($data['file_draf_mou_stmik']) > 0) {
                                                      //  echo'
                                                      //  <li><a  target="_blank" href="../../page/admin/file_draf_mou/' . $data['file_draf_mou_stmik'] . '">View file draf mou stmik</a></li>';
                                                        //<li><a  target="_blank" href="../../page/admin/logo_mou/' . $data['logo_stmik'] . '">View logo stmik</a></li>';
                                                   // } else {
                                                  //  }
                                                     //   echo'<li><a  href="index.php?halaman=14&id=' . $data['id_proposal'] . '" >Kirim draf mou ke stmik</a></li>
                                                   // </ul>
                                                //</div> ';
                                            

                                           // echo '<a title="View Propoasal" href="index.php?halaman=4&id=' . $data['id_proposal'] . '" class="btn-info btn-rounded btn-sm"><span class="glyphicon glyphicon-eye-open"></span></a>';
                                           // echo ' <a title="View Pesan" href="index.php?halaman=7&id=' . $data['id_proposal'] . '" class="btn-warning btn-rounded btn-sm"><span class="glyphicon glyphicon-comment"></span></a>';
                                        } elseif ($data['status_proposal'] == 'Di Proses') {
                                            echo '<a title="View Propoasal" href="index.php?halaman=4&id=' . $data['id_proposal'] . '" class="btn-info btn-rounded btn-sm"><span class="glyphicon glyphicon-eye-open"></span></a>';
                                        } else {
                                            echo '<a title="Edit Proposal" class="btn-warning btn-rounded btn-sm" href="index.php?halaman=5&id=' . $data['id_proposal'] . '"><span class="fa fa-pencil"></span></a>';
                                            echo ' <a title="Delete" class="btn-danger btn-rounded btn-sm" href="index.php?halaman=6&id=' . $data['id_proposal'] . '"><span class="fa fa-times"></span></a>';
                                        } ?>
                                    </td>
                                    <td><?php echo $data['nama_instansi']; ?></td>
                                    <td><?php echo $data['nama_mitra']; ?></td>
                                    <td><?php echo $data['jabatan']; ?></td>
                                    <td><?php echo $data['no_telfon']; ?></td>
                                    <td><?php echo $data['alamat_instansi']; ?></td>
                                                                        
                                    <td><?php echo $data['tgl_pengajuan']; ?></td>
                                    <td><?php echo $data['bidang_kerjasan']; ?></td>
                                    <td>
                                        <a title="View Data Proposal Pengajuan Kerjasama" target="_blank" href="../mitra/file/<?php echo $data['file_proposal']; ?>" class="btn btn-info"><i class="fa fa-folder-open"></i></a>
                                    </td>
                                    <td>
                                        <a title="View Surat Pengantar" target="_blank" href="../mitra/surat_pengantar/<?php echo $data['surat_pengantar']; ?>" class="btn btn-info"><i class="fa fa-folder-open"></i></a>
                                    </td>
                                    
                                    <td><?php echo $data['status_proposal']; ?></td>

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <br>
            <br>
            <i>Info.</i><br>
            <i>1. Info akan diberikan kembali dalam waktu 2 hari, untuk status permohoan diproses..</i><br>
            <i>2. Admin kami akan menghubungi anda jika permohonan kerjasama di sepakati. dengan jangka waktu 2 hari.</i><br>
            <i>3. Pending = anda belum melakukan pengiriman permohonan dengan mencentang (data sudah lengkap).</i><br>
            <i>4. Terkirim = anda sudah melakukan pengiriman permohonan kerjasama.</i><br>
            <!-- <i>5. In Proses = Proposal dalam proses pihak kami (STMIK INDONESIA).</i><br> -->
            <i>5. Di Tolak = permohonan andan di tolak (dengan alasan tertentu).</i>
        </div>
    </div>
</div>