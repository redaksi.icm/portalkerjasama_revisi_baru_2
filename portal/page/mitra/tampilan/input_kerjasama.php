<?php
include "../../config/koneksi.php";
$nama_user = $_SESSION['nama_user'];
$id_user = $_SESSION['id_user'];
if ($_SESSION['id_user'] == null || $_SESSION['id_user'] == 0) {
    header("location:login/error.php");
} else {
}
?>
<?php
if (isset($_POST['simpan'])) {

    $nama_mitra = $_POST['nama_mitra'];
    $jabatan_mitra = $_POST['jabatan_mitra'];
    $no_telfon = $_POST['no_telfon'];
    $nama_instansi = $_POST['nama_instansi'];
    $alamat_instansi = $_POST['alamat_instansi'];
    $tgl_proposal = $_POST['tgl_proposal'];
    $bidang = $_POST['bidang'];
    @$status = $_POST['status_data'];

         
        //surat pengantar
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        $kode_random = substr(str_shuffle($permitted_chars), 0, 5);

        
        $ekstensi_diperbolehkan_surat = array('pdf');
        $nama_surat = $_FILES['surat_pengantar']['name'];
        $surat = $kode_random . '-' . date('Y-m-d') . '-' . $nama_surat;
        $nama_file_db = str_replace('/', '-', $surat);
        $x_surat = explode('.', $surat);
        $ekstensi_surat = strtolower(end($x_surat));
        $ukuran_surat    = $_FILES['surat_pengantar']['size'];
        $file_tmp_surat = $_FILES['surat_pengantar']['tmp_name'];

  

        if (in_array($ekstensi_surat, $ekstensi_diperbolehkan_surat) === true) {
       

            move_uploaded_file($file_tmp_surat, 'surat_pengantar/' . $nama_file_db);
         } else{

         }

    // file proposal 
    $kosong = $ukuran    = $_FILES['file_proposal']['size'];
    if ($kosong == 0) {
        $ekstensi_diperbolehkan = array('pdf', 'pdf');
        $nama = $tgl_proposal . '-' . 'datakosong.pdf';
        $x = explode('.', $nama);
        $ekstensi = strtolower(end($x));
        $nama = 'proposal_kosong.pdf';
    } else {

        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        $kode_random = substr(str_shuffle($permitted_chars), 0, 5);

        $ekstensi_diperbolehkan = array('pdf', 'pdf');
        $nama1 = $_FILES['file_proposal']['name'];
        $nama = $kode_random . '-' . $tgl_proposal . '-' . $nama1;
        $x = explode('.', $nama);
        $ekstensi = strtolower(end($x));
        $ukuran    = $_FILES['file_proposal']['size'];
        $file_tmp = $_FILES['file_proposal']['tmp_name'];
    }

    if ($status != 'Terkirim') {
        $status_data = 'Pending';
    } else {
        $status_data = 'Terkirim';
    }

    if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {

        include "../../config/koneksi.php";
        if ($kosong == 0) {
        } else {
            move_uploaded_file($file_tmp, 'file/' . $nama);
        }
        $simpan = mysqli_query($connect, "INSERT INTO tb_proposal_mitra (id_user, nama_mitra, jabatan, no_telfon, nama_instansi, alamat_instansi, tgl_pengajuan, bidang_kerjasan, file_proposal,surat_pengantar, status_proposal, kategori_proposal,no_mou,no_mou_mitra,status_qusioner) VALUES ('$id_user','$nama_mitra','$jabatan_mitra','$no_telfon','$nama_instansi','$alamat_instansi','$tgl_proposal','$bidang','$nama','$nama_file_db','$status_data','Dalam Negeri','-','-','-')");

        if (!$simpan) {
            echo '<div class="col-md-12">
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>Proses Gagal!</strong> Terdapat kesalahan dalam pengiriman data.
                </div>  
            </div>';
        } else {

            if ($status_data == 'Pending') {
                echo '<div class="col-md-12">
                <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    
                    </button>
                    <strong>Sukses!</strong> Data tersimpan dan tidak terkirim, karna data masih kurang dan belum siap untuk di ajukan <a href="index.php?halaman=3" class="btn btn-danger btn-sm">Lihat Status Proposal</a>
                </div>
            </div>';
            } else {

                echo '<div class="col-md-12">
                <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    
                    </button>
                    <strong>Sukses!</strong> Data berhasil di kirim, kami akan menghubungi anda secepatnya., dan memproses data. kami senang ada sudah menjadi mitra kami!! <a href="index.php?halaman=3" class="btn btn-danger btn-sm">Lihat Status Proposal</a>
                </div>
            </div>';
            }
        }
    } else {
        echo '<div class="col-md-12">
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert">
            <span aria-hidden="true">&times;</span>
            </button>
            <strong>Proses Gagal!</strong> File harus berjenis .PDF
        </div>  
    </div>';
    }
} else {
}
?>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">PENGAJUAN PERMOHONAN KERJASAMA</h3><br><br><br>
                    <font color="red">Wajib isi tanda *</font>
                </div>

                <form action="" method="POST" enctype="multipart/form-data">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label class="col-md-3 control-label" ><font color="red">*</font>Nama Mitra</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" class="form-control" name="nama_mitra" required="required">
                                        </div>
                                        <span class="help-block">.</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><font color="red">*</font>Jabatan</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" class="form-control" name="jabatan_mitra" required="required">
                                        </div>
                                        <span class="help-block">.</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><font color="red">*</font>No. telp/HP</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" class="form-control" name="no_telfon" required="required">
                                        </div>
                                        <span class="help-block">.</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><font color="red">*</font>Nama Instansi/PTS/PTN</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" class="form-control" name="nama_instansi" value="<?php echo $nama_user; ?>">
                                        </div>
                                        <span class="help-block">.</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><font color="red">*</font>Alamat Instansi/PTS/PTN</label>
                                    <div class="col-md-9 col-xs-12">
                                        <textarea class="form-control" rows="5" name="alamat_instansi" required="required"></textarea>
                                        <span class="help-block">.</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><font color="red">*</font>Tanggal Pengajuan</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input type="text" class="form-control datepicker" name="tgl_proposal" value="<?php echo date('Y-m-d'); ?>">
                                        </div>
                                        <span class="help-block">.</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <!-- <label class="col-md-3 control-label">Bidang Kerjasama</label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                            <input type="text" class="form-control" name="bidang">
                                        </div>
                                        <span class="help-block">.</span>
                                    </div> -->
                                        <label class="col-md-3 control-label"><font color="red">*</font>Bidang Kerjasama</label>
                                        <div class="col-md-9">
                                            <select class="" style="width: 375px;height: 30px;" name="bidang">
                                                <option value="0" >Pilih..</option>
                                                <option value="bidang pendidikan dan pengajaran">bidang pendidikan dan pengajaran</option>
                                                <option value="bidang penelitian dan pengembangan ilmu">bidang penelitian dan pengembangan ilmu</option>
                                                <option value="bidang pengabdian kepada masyarakat">bidang pengabdian kepada masyarakat</option>
                                                <option value="bidang kegiatan lainnya">bidang kegiatan lainnya</option>
                                                

                                            
                                            </select>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <br>
                                    <br><br><br><br><br>
                                    <label class="col-md-3 control-label"><font color="red">*</font>Upload Surat Pengantar</label>
                                    <div class="col-md-9">
                                        <input type="file" class="fileinput btn-primary" name="surat_pengantar" id="surat_pengantar" title="Browse file" required="required">
                                        <span class="help-block">File berbentuk .PDF</span>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <br>
                                    <br><br><br>
                                    <label class="col-md-3 control-label">File Proposal Kerjasama</label>
                                    <div class="col-md-9">
                                        <input type="file" class="fileinput btn-primary" name="file_proposal" id="file_proposal" title="Browse file">
                                        <span class="help-block">File berbentuk .PDF</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label"></label>
                                    <div class="col-md-9">
                                        <label class="check" style="color:red;"><input type="checkbox" class="icheckbox" name="status_data" value="Terkirim"  /> Centang data sudah lengkap.</label>
                                        <span class="help-block" style="color:black;">Dengan mencentang, maka data tidak bisa di edit lagi dan data akan dikirim.</span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <div class="form-group">
                            <button class="btn btn-primary pull-left" name="simpan">Kirim Permohonan<span class="fa fa-floppy-o fa-right"></span></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>