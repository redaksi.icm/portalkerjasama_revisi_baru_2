<?php
include "../../config/koneksi.php";
$nama_user = $_SESSION['nama_user'];
$id_user = $_SESSION['id_user'];
if ($_SESSION['id_user'] == null || $_SESSION['id_user'] == 0) {
    header("location:login/error.php");
} else {
}
?>

<?php
if (isset($_POST['simpan'])) {

    $id_pertanyaan = $_POST['id_pertanyaan'];
    $pilihan = $_POST['pilihan'];
    $id_user = $_SESSION['id_user'];
    $id_proposal = $_GET['id'];

    $simpan = mysqli_query($connect, "INSERT INTO tb_jawaban (id_pertanyaan, id_user, id_proposal, jawaban) 
                    VALUES ('$id_pertanyaan','$id_user','$id_proposal','$pilihan')");

    if (!$simpan) {
        echo '<div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
                </button>
                <strong>Proses Gagal!</strong> Terdapat kesalahan dalam pengiriman data.
            </div>  
        </div>';
    } else {
        echo '<div class="col-md-12">
            <div class="alert alert-success" role="alert">
            <a href="index.php?halaman=11&id=' . $id_proposal . '" class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>                    
                </a>
                <strong>Sukses!</strong> Next...
            </div>
        </div>';
    }
} elseif (isset($_POST['simpankan'])) {
    $id_pertanyaan_saran = $_POST['id_pertanyaan_saran'];
    $isian_saran = $_POST['isian_saran'];
    $id_user = $_SESSION['id_user'];
    $id_proposal = $_GET['id'];


    $simpan1 = mysqli_query($connect, "INSERT INTO tb_jawaban_saran (id_pertanyaan_saran, id_user, id_proposal, jawaban_saran) 
                    VALUES ('$id_pertanyaan_saran','$id_user','$id_proposal','$isian_saran')");

    if (!$simpan1) {
        echo '<div class="col-md-12">
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert">
            <span aria-hidden="true">&times;</span>
            </button>
            <strong>Proses Gagal!</strong> Terdapat kesalahan dalam pengiriman data.
        </div>  
    </div>';
    } else {
        echo '<div class="col-md-12">
        <div class="alert alert-success" role="alert">
        <a href="index.php?halaman=11&id=' . $id_proposal . '" class="close" data-dismiss="alert">
            <span aria-hidden="true">&times;</span>                    
            </a>
            <strong>Sukses!</strong> Next...
        </div>
    </div>';
    }
}
?>

<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">QUSIONER</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>

                <div class="panel-body">
                    Petunjuk pengisian <br>
                    <i>Berilah tanda √ pada koloom 1 s.d 5 sesuai penilaian Bapak / Ibu, dangan keterangan sebagai
                        berikut: <br>
                        1 : Kurang Sekali <br>
                        2 : Kurang <br>
                        3 : Sedang <br>
                        4 : Baik <br>
                        5 : Baik Sekali</i><br>
                    <br>
                    <br>
                    <?php
                    $no = 1;
                    $hasil1 = mysqli_fetch_array(mysqli_query($connect, "SELECT count(id_pertanyaan) as jumlah from tb_pertanyaan_kusioner WHERE id_pertanyaan NOT IN (SELECT id_pertanyaan FROM tb_jawaban where id_user='$id_user' and id_proposal='$_GET[id]')"));

                    $tampilkan = mysqli_query($connect, "SELECT * FROM tb_pertanyaan_kusioner WHERE id_pertanyaan NOT IN (SELECT id_pertanyaan FROM tb_jawaban where id_user='$id_user' and id_proposal='$_GET[id]') ORDER BY RAND () limit 1");

                    foreach ($tampilkan as $data) {
                    ?>

                        <form action="" method="POST">
                            <div class="col-md-8">
                                <div class="panel panel-default">
                                    <div class="item item-visible">
                                        <div class="text panel-body">
                                            <div class="heading">
                                                <a href="#">Sisa Pertanyaan: </a>
                                                <span class="date"><?php echo  $hasil1['jumlah']; ?> </span>
                                            </div>
                                            <br>
                                            <font color="red">Pertanyaan</font> : <?php echo $data['kategori_pertanyaan']; ?>
                                            <input type="hidden" name="id_pertanyaan" value="<?php echo $data['id_pertanyaan']; ?>">
                                        </div>
                                    </div>

                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label class="check"><input type="radio" class="iradio" name="pilihan" value="1" /> 1. Kurang Sekali</label>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="check"><input type="radio" class="iradio" name="pilihan" value="2" /> 2. Kurang</label>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="check"><input type="radio" class="iradio" name="pilihan" value="3" /> 3. Sedang</label>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="check"><input type="radio" class="iradio" name="pilihan" value="4" /> 4. Baik</label>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="check"><input type="radio" class="iradio" name="pilihan" value="5" checked="checked" /> 5. Baik Sekali</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-danger btn-rounded" name="simpan">Kirim..</button>
                                </div>
                            </div>
                        </form>
                    <?php } ?>

                    <?php if ($hasil1['jumlah'] == 0) { ?>
                        <?php
                        $hasil2 = mysqli_fetch_array(mysqli_query($connect, "SELECT count(id_pertanyaan_saran) as jumlahkan from tb_pertanyaan_saran WHERE id_pertanyaan_saran NOT IN (SELECT id_pertanyaan_saran FROM tb_jawaban_saran where id_user='$id_user' and id_proposal='$_GET[id]')"));

                        $tampilkan = mysqli_query($connect, "SELECT * FROM tb_pertanyaan_saran WHERE id_pertanyaan_saran NOT IN (SELECT id_pertanyaan_saran FROM tb_jawaban_saran where id_user='$id_user' and id_proposal='$_GET[id]') ORDER BY RAND () limit 1");
                        foreach ($tampilkan as $data1) {
                        ?>
                        <form action="" method="POST">
                            <div class="col-md-8 center">
                                <div class="panel panel-default">
                                    <div class="panel-heading ui-draggable-handle">
                                        <h3 class="panel-title">Rencana tindak lanjut dan saran perbaikan</h3>
                                        <br>
                                        
                                    </div>
                                    <i>Sisa pertanyaan : <?php echo  $hasil2['jumlahkan']; ?> </i>
                                    <div class="panel-body">
                                        <ul class="list-group border-bottom">
                                            <li class="list-group-item"><?php echo $data1['pertanyaan_saran']; ?></li>
                                            <input type="hidden" name="id_pertanyaan_saran" value="<?php echo $data1['id_pertanyaan_saran']; ?>">
                                            <br>
                                            <label>Kolom Isian </label>
                                            <textarea class="form-control" rows="5" style="border-color: red;" name="isian_saran"></textarea>
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-danger btn-rounded" name="simpankan">Kirim..</button>
                                </div>
                            </div>
                        </form>

                        <?php } ?>

                        <div class="col-md-3 center">
                        </div>
                       
                    <?php } ?>
                    <?php if ( @$hasil2['jumlahkan']==0 && $hasil1['jumlah'] == 0) { ?>
                    <div class="col-md-6 center">
                            <div class="alert alert-warning text-center" role="alert">
                                <strong class=''> TERIMAKASIH!</strong> SEMOGA KAMI BISA LEBIH BAIK LAGI.
                                <br>
                                ANDAN TELAH SELESAI MENGISI QUSIONER KAMI
                                <a href="index.php?halaman=12&id=<?php echo $_GET['id'];?>" class="btn btn-primary btn-block">KLIK SELESAI</a>
                            </div>
                        </div>
                    <?php } ?>
                </div>

            </div>
        </div>
    </div>
</div>