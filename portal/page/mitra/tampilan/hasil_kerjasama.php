<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">HASIL DAN KELANJUTAN KERJASAMA</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>

                <?php
                $no = 1;

                $tampilkan = mysqli_query($connect, "SELECT * FROM tb_hasil_kerjasama where id_proposal='$_GET[id]'");

                foreach ($tampilkan as $data) {
                    $hasil = $data['hasil_kerjasama'];
                    $hasil2 = $data['kerjasama_selanjutnya'];
                }
                ?>

                <form action="" method="POST">
                    <div class="col-md-12">

                        <div class="block">
                            <h4> 1. Manfaat Yang Telah Diperoleh </h4>
                            <div class="panel-body" style="background: lavender;">
                                <?php echo @$hasil; ?>
                            </div>
                        </div>

                        <div class="block">
                            <h4> 2. Perencanaan Kerjaasama Berikutnya</h4>
                            <div class="panel-body" style="background: lavender;">
                                <?php echo @$hasil2; ?>
                            </div>
                        </div>
                        <div class="panel-footer">
                             <a href="index.php?halaman=10" class="btn btn-primary pull-left">Kembali <span class="fa fa fa-mail-reply-all"></span></a>
                            <!-- <button class="btn btn-default">Clear Form</button> -->
                            <!-- <button class="btn btn-primary pull-right" name="simpan" type="submit">Simpan</button> -->
                        </div>

                    </div>
                </form>

            </div>
        </div>
    </div>