<?php
include "../../config/koneksi.php";
$nama_user = $_SESSION['nama_user'];
$id_user = $_SESSION['id_user'];
if ($_SESSION['id_user'] == null || $_SESSION['id_user'] == 0) {
    header("location:login/error.php");
} else {
}


if (@$_GET['session'] == 'Qusioner_selesai') {
    echo '<div class="col-md-12">
            <div class="alert alert-success" role="alert">
            <a href="index.php?halaman=10" class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>                    
                </a>
                <strong>Sukses!</strong> Terimakasih, Anda sudah menyelesaikan 1 qusioner.
            </div>
        </div>';
} else {
    echo '';
}
?>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">HASIL DAN QUSIONER </h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>

                <div class="panel-body">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>Action</th>
                                <th>No Mou STMIK</th>
                                <th>No Mou Mitra</th>
                                <th>File Laporan</th>
                                <th>Tgl TTD</th>
                                <th>Tgl Pengajuan</th>
                                <th>Masa Berlaku</th>
                                <th>Nama Mitra</th>
                                <th>Status Qusioner</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampilkan = mysqli_query($connect, "SELECT tb_proposal_mitra.*, tb_file_mou.* FROM tb_proposal_mitra INNER JOIN tb_file_mou on tb_file_mou.id_proposal=tb_proposal_mitra.id_proposal where tb_proposal_mitra.status_proposal='Di Setujui' and tb_proposal_mitra.id_user = '$_SESSION[id_user]' and tb_proposal_mitra.status_qusioner != '-' ORDER BY tb_proposal_mitra.id_proposal DESC");

                            foreach ($tampilkan as $data) {
                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false">Action <span class="caret"></span></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="index.php?halaman=13&id=<?php echo $data['id_proposal']; ?>&session=pesan">Hasil Kerjasama</a></li>
                                                <li><a href="index.php?halaman=4&id=<?php echo $data['id_proposal']; ?>&session=qusioner_view">View Data</a></li>
                                                <?php if ($data['status_qusioner'] != 'Selesai') { ?>
                                                    <li><a href="index.php?halaman=11&id=<?php echo $data['id_proposal']; ?>">Isi Qusioner</a></li>
                                                <?php } else { ?>

                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </td>
                                    <td><a href="../../page/admin/file_mou/<?php echo $data['file_mou_stmik']; ?>" target="_blank"><?php echo $data['no_mou']; ?></a></td>
                                    <td><a href="../../page/admin/file_mou/<?php echo $data['file_mou_stmik']; ?>" target="_blank"><?php echo $data['no_mou_mitra']; ?></a></td>
                                    <td>
                                        <?php

                                        if (empty($data['laporan_kerja_sama'])) {
                                            echo 'Data Kosong';
                                        } else {
                                            echo '<a href="../../page/admin/file_laporan/' . $data["laporan_kerja_sama"] . '" target="_blank">Download</a>';
                                        }
                                        ?>

                                    </td>
                                    <td><?php echo $data['tgl_mou']; ?></td>
                                    <td><?php echo $data['tgl_pengajuan']; ?></td>
                                    <td><?php echo $data['masa_berlaku']; ?></td>
                                    <td><?php echo $data['nama_mitra']; ?></td>
                                    <td><?php if ($data['status_qusioner'] != 'Selesai') { ?>
                                            Belum Di Isi
                                        <?php } else { ?>
                                            Sudah di isi
                                        <?php } ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>