<?php
include "../../config/koneksi.php";
$nama_user = $_SESSION['nama_user'];
$id_user = $_SESSION['id_user'];
if ($_SESSION['id_user'] == null || $_SESSION['id_user'] == 0) {
    header("location:login/error.php");
} else {
}
?>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">KERJASAMA DALAM PELAKSANAAN</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>Action</th>
                                <th>No Mou STMIK</th>
                                <th>No Mou Mitra</th>
                                <th>Masa Berlaku</th>
                                <th>Nama Mitra</th>
                                <th>Jabatan</th>
                                <!-- <th>Nama Instansi</th> -->
                                <th>Tgl TTD</th>
                                <!-- <th>Bidang Kerjasama</th> -->
                                <!-- <th>File</th> -->
                                <!-- <th>Kategori</th> -->
                                <th>Status Qusioner</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampilkan = mysqli_query($connect, "SELECT tb_proposal_mitra.*, tb_file_mou.* FROM tb_proposal_mitra INNER JOIN tb_file_mou on tb_file_mou.id_proposal=tb_proposal_mitra.id_proposal where tb_proposal_mitra.status_proposal='Di Setujui' and no_mou != '-' and tb_proposal_mitra.id_user='$id_user'  ORDER BY tb_proposal_mitra.id_proposal DESC");
                            foreach ($tampilkan as $data) {

                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false">Action <span class="caret"></span></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="index.php?halaman=9&id=<?php echo $data['id_proposal'];?>">INPUT/ EDIT MOU</a></li>
                                            </ul>
                                        </div>
                                    <td><a href="../../page/admin/file_mou/<?php echo $data['file_mou_stmik'];?>" target="_blank"><?php echo $data['no_mou']; ?></a></td>
                                    <td><a href="../../page/admin/file_mou/<?php echo $data['file_mou_mitra'];?>" target="_blank"><?php echo $data['no_mou_mitra']; ?></a></td>
                                    <td><?php echo $data['masa_berlaku']; ?></td>
                                    <td><?php echo $data['nama_mitra']; ?></td>
                                    <td><?php echo $data['jabatan']; ?></td>
                                    <!-- <td><?php echo $data['nama_instansi']; ?></td> -->
                                    <td><?php echo $data['tgl_mou']; ?></td>
                                    <!-- <td><?php echo $data['bidang_kerjasan']; ?></td> -->
                                    <!-- <td>
                                        <a title="View Data Proposal Pengajuan Kerjasama" target="_blank" href="../mitra/file/<?php echo $data['file_proposal']; ?>" class="btn btn-info"><i class="fa fa-folder-open"></i></a>
                                    </td> -->
                                    <!-- <td><?php echo $data['kategori_proposal']; ?></td> -->
                                    <td><?php if($data['status_qusioner'] == 'Terkirim') { ?>
                                            Diterima
                                        <?php }elseif($data['status_qusioner'] == 'Selesai'){ ?>
                                            Sudah DI Isi
                                        <?php }else{ ?>
                                            Belum di terima
                                        <?php } ?>
                                        </td>

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <br>
            <br>
            <!-- <i>Info.</i><br>
            <i>1. Info akan diberikan kembali dalam waktu 2 hari, untuk status proposal di proses..</i><br>
            <i>2. Admin kami akan menghubungi anda jika proposal kerjasama di sepakati. dengan jangka waktu 2 hari.</i><br>
            <i>3. Pending = anda belum melakukan pengiriman proposal dengan mencentang (data sudah lengkap).</i><br>
            <i>4. Terkirim = anda sudah melakukan pengiriman proposal.</i><br>
            <i>5. Di Tolak = Proposal andan di tolak (dengan alasan tertentu).</i> -->
        </div>
    </div>
</div>