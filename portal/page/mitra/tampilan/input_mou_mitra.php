<?php
if (isset($_POST['simpan'])) {
    $no_mou_mitra = $_POST['no_mou_mitra'];
    $ukuran    = $_FILES['file_mou_mitra']['size'];
    $ttd = $_POST['ttd_mou_stmik'];

    if ($ukuran == 0) {
        $ekstensi_diperbolehkan = array('pdf', 'pdf');
        $kode = $_POST['file_mou_mitra_edit'];
        $ekstensi = 'pdf';
    } else {

        $ekstensi_diperbolehkan = array('pdf', 'pdf');
        $nama1 = $_FILES['file_mou_mitra']['name'];
        $nama = $no_mou_mitra . 'mitra' . '-' . date('Y-m-d') . '-' . $nama1;
        $kode=str_replace('/','-', $nama);
        $x = explode('.', $nama);
        $ekstensi = strtolower(end($x));
        $ukuran    = $_FILES['file_mou_mitra']['size'];
        $file_tmp = $_FILES['file_mou_mitra']['tmp_name'];
    }

    if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
        if ($ukuran == 0) {
        } else {
            move_uploaded_file($file_tmp, '../../page/admin/file_mou/' . $kode);
        }
        $updatekan = mysqli_query($connect, "UPDATE tb_proposal_mitra SET  no_mou_mitra='$no_mou_mitra' where id_proposal ='$_GET[id]'");
        $updatekan1 = mysqli_query($connect, "UPDATE tb_file_mou SET file_mou_mitra='$kode', ttd_mitra='$ttd' where id_proposal ='$_GET[id]'");

        if ($connect) {
            echo '<div class="col-md-12">
            <div class="alert alert-success" role="alert">
            <a href="index.php?admin=5" class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>                    
                </a>
                <strong>Sukses!</strong> Nomor MOU berhasil di inputkan <a href="index.php?halaman=8" class="btn btn-danger btn-sm">Lihat Proposal</a>
            </div>
        </div>';
        } else {
            echo '<div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <a href="index.php?admin=5" class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
                </a>
                <strong>Proses Gagal!</strong> Ada kesalahan sistem.
            </div>  
        </div>';
        }
    } else {
        echo '<div class="col-md-12">
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert">
            <span aria-hidden="true">&times;</span>
            </button>
            <strong>Proses Gagal!</strong> File harus berjenis .PDF
        </div>  
    </div>';
    }
}

$kanya = mysqli_query($connect, "SELECT * from tb_file_mou where id_proposal ='$_GET[id]'");
$rowkan = mysqli_fetch_array($kanya);
if (mysqli_num_rows($kanya) == 1) {
    $tampilkan = mysqli_query($connect, "SELECT tb_proposal_mitra.*, tb_file_mou.* FROM tb_proposal_mitra INNER JOIN tb_file_mou on tb_file_mou.id_proposal=tb_proposal_mitra.id_proposal where tb_proposal_mitra.id_proposal='$_GET[id]'");
} else {

    $tampilkan = mysqli_query($connect, "SELECT * FROM tb_proposal_mitra where id_proposal ='$_GET[id]'");
}

foreach ($tampilkan as $data) {

?>
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">PENGIMPUTAN NOMOR MOU MITRA</h3>
                    </div>
                    <form action="" method="POST" enctype="multipart/form-data">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Nomor MOU</label>
                                        <div class="col-md-10">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="no_mou_mitra" value="<?php echo $data['no_mou_mitra']; ?>">
                                            </div>
                                            <span class="help-block">.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Nama TTD</label>
                                        <div class="col-md-10">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="ttd_mou_stmik" value="<?php echo $data['ttd_mitra']; ?>">
                                            </div>
                                            <span class="help-block">.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Tanggal Mou</label>
                                        <div class="col-md-10">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" class="form-control" name="tgl_mou_stmik" value="<?php echo $data['tgl_mou']; ?>" style="color: red;" readonly>
                                            </div>
                                            <span class="help-block">.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Masa Berlaku</label>
                                        <div class="col-md-10">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" class="form-control" name="" value="<?php echo $data['masa_berlaku']; ?>" style="color: red;" readonly>
                                            </div>
                                            <span class="help-block">.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">File MOU</label>
                                        <div class="col-md-10">
                                            <input type="file" class="fileinput btn-primary" name="file_mou_mitra" id="file_mou_mitra" title="Browse file">
                                            <br>
                                            <input type="hidden" class="form-control" name="file_mou_mitra_edit" id="file_mou_mitra_edit" value="<?php echo $data['file_mou_mitra']; ?>">
                                            <span class="help-block">File berbentuk .PDF</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group">
                                <button class="btn btn-primary pull-left" name="simpan">SAVE<span class="fa fa-floppy-o fa-right"></span></button>
                                <!-- <a href="index.php?admin=5" class="btn btn-primary pull-right" name="simpan">Kembali <span class="fa fa fa-mail-reply-all"></span></a> -->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>