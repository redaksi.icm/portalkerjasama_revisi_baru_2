<div class="page-content-wrap">
    <!-- START WIDGETS -->
    <div class="row">

        <div class="col-md-12">

            <!-- START WIDGET CLOCK -->
            <div class="widget widget-info widget-padding-sm">
                <div class="widget-big-int plugin-clock">00:00</div>
                <div class="widget-subtitle plugin-date">Loading...</div>
                <div class="widget-controls">
                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="left" title="Remove Widget"><span class="fa fa-times"></span></a>
                </div>
                <div class="widget-buttons widget-c3">
                    <div class="col">
                        <a href="#"><span class="fa fa-clock-o"></span></a>
                    </div>
                    <div class="col">
                        <a href="#"><span class="fa fa-bell"></span></a>
                    </div>
                    <div class="col">
                        <a href="#"><span class="fa fa-calendar"></span></a>
                    </div>
                </div>
            </div>
            <!-- END WIDGET CLOCK -->

        </div>
        <div class="col-md-12 scCol ui-sortable">

            <div class="panel panel-success" id="grid_block_5">
                <div class="panel-heading ui-draggable-handle ui-sortable-handle">
                    <h2 class="panel-title">Selamat Datang...!!<br>
                        <style type="text/css">

                            a:hover {
                                  font-weight: bold;
                                  color: green;
                            }
                        </style>
                        Terimakasih anda telah mendaftar sebagai mitra kami.  
                        Silahkan ajukan permohonan kerjasama anda di  menu <a href="index.php?halaman=2"><u>PENGAJUAN  PERMOHONAN. </u></a> 
                        Anda dapat melihat proses tahapan kerjasama pada gambar di bawah ini.
                    </h2>
                </div>
                <div class="panel-body">
                    <h2>PROSES TAHAPAN KERJASAMASA</h2>
                    <div align="center"><img src="file/proseskerjasama1.jpg"/>
                     </div>
              </div>
            </div>

        </div>
    </div>
    <!-- END WIDGETS -->

    <div class="row">
        <div class="col-md-4">

            <!-- START USERS ACTIVITY BLOCK -->
            <!-- END USERS ACTIVITY BLOCK -->

        </div>
        <div class="col-md-4">

            <!-- START VISITORS BLOCK -->

            <!-- END VISITORS BLOCK -->

        </div>

        <div class="col-md-4">

            <!-- START PROJECTS BLOCK -->
            <!-- END PROJECTS BLOCK -->

        </div>
    </div>

    <div class="row">
        <div class="col-md-8">

            <!-- START SALES BLOCK -->
            <!-- END SALES BLOCK -->

        </div>
        <div class="common-modal modal fade" id="common-Modal1" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-content">
                <ul class="list-inline item-details">
                    <li><a href="http://themifycloud.com/downloads/janux-premium-responsive-bootstrap-admin-dashboard-template/">Admin templates</a></li>
                    <li><a href="http://themescloud.org">Bootstrap themes</a></li>
                </ul>
            </div>
        </div>

        <div class="col-md-4">

            <!-- START SALES & EVENTS BLOCK -->

            <!-- END SALES & EVENTS BLOCK -->

        </div>
    </div>

    <!-- START DASHBOARD CHART -->
    <div class="chart-holder" id="dashboard-area-1" style="height: 200px;"></div>
    <div class="block-full-width">

    </div>
    <!-- END DASHBOARD CHART -->

</div>