<?php
include "../../config/koneksi.php";
$nama_user = $_SESSION['nama_user'];
$id_user = $_SESSION['id_user'];
if ($_SESSION['id_user'] == null || $_SESSION['id_user'] == 0) {
    header("location:login/error.php");
} else {
}
?>
<div class="page-sidebar" >
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation">
        <li class="xn-logo">
            <a href="index.php">STMIK IP</a>
            <a href="#" class="x-navigation-control"></a>
        </li>
        <li class="xn-profile">
            <a href="#" class="profile-mini">
                <img src="../../gambar/logo2.jpg" alt="admin" />
            </a>
            <div class="profile">
                <div class="profile-image">
                    <img src="../../gambar/logo2.jpg" alt="admin" />
                </div>
                <div class="profile-data">
                    <div class="profile-data-name"><?php echo $_SESSION['nama_user']; ?></div>
                    <div class="profile-data-title">Anda mitra kami..</div>
                </div>
                <!-- <div class="profile-controls">
                    <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                    <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                </div> -->
            </div>
        </li>
        <li class="xn-title" style="color: white">Navigation</li>
        <li class="">
            <a href="index.php?halaman=1"><span class="fa fa-desktop"></span> <span class="xn-text" style="color: white">Dashboard</span></a>
        </li>
        <li><a href="index.php?halaman=2"><span class="fa fa-file-text-o"></span> <span class="xn-tet" style="color: white"> Pengajuan Permohonan</a></li>
        <li><a href="index.php?halaman=3"><span class="fa fa-folder-open-o"></span> <span class="xn-text" style="color: white"> Data Permohonan</a></li>
        <li><a href="index.php?halaman=8"><span class="fa fa-folder-open-o"></span> <span class="xn-text" style="color: white"> Pelaksanaan Kerjasama</a></li>
        <li><a href="index.php?halaman=10"><span class="fa fa fa-comments"></span> <span class="xn-text" style="color: white"> Hasil dan Qusioner</a></li>
        <li class="xn-title" style="color: white">Setting</li>
        <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> <span class="xn-text" style="color: white"> Logout</a></li>

    </ul>
    <!-- END X-NAVIGATION -->
</div>

<!-- batas -->

<div class="page-content">
    <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
        <li class="xn-icon-button">
            <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
        </li>
        <li class="xn-icon-button pull-right">
            <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>
        </li>
        <!-- END SIGN OUT -->
        <!-- MESSAGES -->
       <!-- <li class="xn-icon-button pull-right">
            <a href="#"><span class="fa fa-comments"></span></a>
            <?php
            $query = mysqli_fetch_array(mysqli_query($connect, "SELECT count(di_catatan) as total_new FROM tb_catatan where status='terkirim' and id_user='$id_user'"));
            ?>
            <div class="informer informer-danger"><?php echo "$query[total_new]"; ?> </div>
            <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                <div class="panel-heading">
                    <h3 class="panel-title"><span class="fa fa-comments"></span> Messages/ Catatan</h3>
                    <div class="pull-right">
                        <span class="label label-danger"><?php echo "$query[total_new]"; ?> new</span>
                    </div>
                </div>
                <div class="panel-body list-group list-group-contacts scroll" style="height: 200px;">
                    <?php
                    $no = 1;
                    $tampilkan = mysqli_query($connect, "SELECT tb_proposal_mitra.*, tb_catatan.catatan FROM tb_proposal_mitra LEFT JOIN tb_catatan on tb_proposal_mitra.id_proposal=tb_catatan.id_proposal where tb_proposal_mitra.id_user = '$id_user' and tb_catatan.status='terkirim'");
                    foreach ($tampilkan as $data) {
                    ?>
                        <a href="index.php?halaman=7&id=<?php echo $data['id_proposal'];?>" class="list-group-item">
                            <div class="list-group-status status-online"></div>
                            <img src="../../gambar/logo.jpeg" class="pull-left" alt="admin" style="height: 50px; width: 50px" />
                            <span class="contacts-title">Dari Admin</span>
                            <p>Kerjasama: <?php echo $data['bidang_kerjasan']; ?></p>
                            <p>Catatan: <?php echo $data['catatan']; ?></p>
                        </a>
                    <?php } ?>

                </div>
                <div class="panel-footer text-center">
                    <a href="pages-messages.html">Show all messages</a>
                </div>
            </div>
        </li>-->
    </ul>
    <ul class="breadcrumb">
    </ul>