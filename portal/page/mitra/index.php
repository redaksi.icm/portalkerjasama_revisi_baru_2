<?php 
include"../../config/koneksi.php";
session_start();
$nama_user = $_SESSION['nama_user'];
$id_user = $_SESSION['id_user'];
if($_SESSION['id_user'] == null || $_SESSION['id_user'] == 0){
    header("location:login/error.php");
}else{

}
?>
<?php include 'head.php'; ?>
<?php include 'head_sub.php'; ?>
<!-- batas -->
<div class="page-container">
    <!-- START PAGE SIDEBAR -->
    <?php include 'sidebar.php'; ?>
    <!-- END PAGE SIDEBAR -->
    <?php include 'login/loading.php'; ?>
    <!-- PAGE CONTENT -->
    <!-- END PAGE CONTENT -->
    <!-- tempat loading seharusnya -->
    <!-- END PAGE CONTENT WRAPPER -->
    
</div>
</div>

<!-- END PAGE CONTAINER -->
<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
            <div class="mb-content">
                <p>Apa kamu yakin untuk log out?</p>
                <p>klik No untuk batal.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <a href="login/logout.php" class="btn btn-success btn-lg">Yes</a>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- batas -->
<?php include 'footer.php'; ?>