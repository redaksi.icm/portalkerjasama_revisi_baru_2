<audio id="audio-alert" src="../../desain/audio/alert.mp3" preload="auto"></audio>
<audio id="audio-fail" src="../../desain/audio/fail.mp3" preload="auto"></audio>
<script type="text/javascript" src="../../desain/js/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="../../desain/js/plugins/jquery/jquery-ui.min.js"></script>
<script type="text/javascript" src="../../desain/js/plugins/bootstrap/bootstrap.min.js"></script>
<script type='text/javascript' src='../../desain/js/plugins/icheck/icheck.min.js'></script>
<script type="text/javascript" src="../../desain/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>



<script type="text/javascript" src="../../desain/js/plugins/codemirror/codemirror.js"></script>   
<script type='text/javascript' src="../../desain/js/plugins/codemirror/mode/htmlmixed/htmlmixed.js"></script>
<script type='text/javascript' src="../../desain/js/plugins/codemirror/mode/xml/xml.js"></script>
<script type='text/javascript' src="../../desain/js/plugins/codemirror/mode/javascript/javascript.js"></script>
<script type='text/javascript' src="../../desain/js/plugins/codemirror/mode/css/css.js"></script>
<script type='text/javascript' src="../../desain/js/plugins/codemirror/mode/clike/clike.js"></script>
<script type='text/javascript' src="../../desain/js/plugins/codemirror/mode/php/php.js"></script>    

<script type="text/javascript" src="../../desain/js/plugins/summernote/summernote.js"></script>


<script type="text/javascript" src="../../desain/js/plugins/datatables/jquery.dataTables.min.js"></script>

<script type="text/javascript" src="../../desain/js/plugins/knob/jquery.knob.min.js"></script>

<script type="text/javascript" src="../../desain/js/plugins/scrolltotop/scrolltopcontrol.js"></script>
<script type="text/javascript" src="../../desain/js/plugins/morris/raphael-min.js"></script>
<script type="text/javascript" src="../../desain/js/plugins/morris/morris.min.js"></script>
<script type="text/javascript" src="../../desain/js/plugins/rickshaw/d3.v3.js"></script>
<script type="text/javascript" src="../../desain/js/plugins/rickshaw/rickshaw.min.js"></script>
<script type='text/javascript' src='../../desain/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'></script>
<script type='text/javascript' src='../../desain/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'></script>
<script type='text/javascript' src='../../desain/js/plugins/bootstrap/bootstrap-datepicker.js'></script>
<script type="text/javascript" src="../../desain/js/plugins/owl/owl.carousel.min.js"></script>
<script type="text/javascript" src="../../desain/js/plugins/moment.min.js"></script>
<script type="text/javascript" src="../../desain/js/plugins/daterangepicker/daterangepicker.js"></script>
<script type='text/javascript' src='../../desain/js/plugins/noty/jquery.noty.js'></script>
<script type='text/javascript' src='../../desain/js/plugins/noty/layouts/topCenter.js'></script>
<script type='text/javascript' src='../../desain/js/plugins/noty/layouts/topLeft.js'></script>
<script type='text/javascript' src='../../desain/js/plugins/noty/layouts/topRight.js'></script>
<script type='text/javascript' src='../../desain/js/plugins/noty/themes/default.js'></script>
<script type="text/javascript" src="../../desain/js/plugins/tagsinput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="../../desain/js/plugins/bootstrap/bootstrap-select.js"></script>
<script type="text/javascript" src="../../desain/js/plugins/bootstrap/bootstrap-file-input.js"></script>


<script>
    var editor = CodeMirror.fromTextArea(document.getElementById("codeEditor"), {
        lineNumbers: true,
        matchBrackets: true,
        mode: "application/x-httpd-php",
        indentUnit: 4,
        indentWithTabs: true,
        enterMode: "keep",
        tabMode: "shift"
    });
    editor.setSize('100%', '420px');
</script>


<script type="text/javascript">
    function notyConfirm() {
        noty({
            text: 'Do you want to continue?',
            layout: 'topRight',
            buttons: [{
                    addClass: 'btn btn-success btn-clean',
                    text: 'Ok',
                    onClick: function($noty) {
                        $noty.close();
                        noty({
                            text: 'You clicked "Ok" button',
                            layout: 'topRight',
                            type: 'success'
                        });
                    }
                },
                {
                    addClass: 'btn btn-danger btn-clean',
                    text: 'Cancel',
                    onClick: function($noty) {
                        $noty.close();
                        noty({
                            text: 'You clicked "Cancel" button',
                            layout: 'topRight',
                            type: 'error'
                        });
                    }
                }
            ]
        })
    }
</script>
<!-- END PAGE PLUGINS -->

<!-- START TEMPLATE -->
<script type="text/javascript" src="../../desain/js/settings.js"></script>
<script type="text/javascript" src="../../desain/js/plugins.js"></script>
<script type="text/javascript" src="../../desain/js/actions.js"></script>
<!-- END TEMPLATE -->
<!-- END SCRIPTS -->

</body>

</html>