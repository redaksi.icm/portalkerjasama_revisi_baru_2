<!DOCTYPE html>
<html lang="en" class="body-full-height">

<head>
    <!-- META SECTION -->
    <title>PORTAL - KERJASAMA STMIK IP</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="desain/css/theme-default.css" />
    <!-- EOF CSS INCLUDE -->
</head>

<body>
    <div class="login-container lightmode" style="background: #00FF7F;">
        <div class="login-box animated fadeInDown">
            <div class="login-logo"></div>
            <div class="login-body" style="background: #1b1e24;">
                <div class="login-title"><strong>Welcome</strong>, Please login all</div>
                <form action="page/proses_login.php" class="form-horizontal" method="POST">
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="text" class="form-control" style="background: aliceblue;" name="username_email" placeholder="Username/ Email" required="required" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="password" class="form-control" style="background: aliceblue;" name="password" placeholder="Password" required="required" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <select class="form-control" name="status" id="status">
                                <option value="0">-- Pilih --</option>
                                <option value="1">Ketua</option>
                                <option value="2">Admin</option>
                                <option value="3">Mitra</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <a href="../index.php" class="btn btn-link btn-block">Back to website!</a>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-info btn-block">Log In</button>
                        </div>
                    </div>
                    <div class="login-subtitle">
                        Belum Memiliki Akun? <a href="register.php">
                            <font color="#00FF7F">Create an account
                        </a></font>
                    </div>

                </form>
            </div>
            <div class="login-footer">
                <div class="pull-left">
                    <font color="black">Create by: 2020 STMIK INDONESIA PADANG</font>
                </div>
            </div>
        </div>

    </div>

</body>

</html>