<!DOCTYPE html>
<html lang="en" class="body-full-height">

<head>
    <!-- META SECTION -->
    <title>PORTAL - KERJASAMA STMIK IP</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="desain/css/theme-default.css" />
    <!-- EOF CSS INCLUDE -->
</head>

<body>
    <?php

    // library email ini harus dipanggil sesuai directorynya diletakkan


    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    require_once "../library/PHPMailer.php";
    require_once "../library/Exception.php";
    require_once "../library/OAuth.php";
    require_once "../library/POP3.php";
    require_once "../library/SMTP.php";
    $mail = new PHPMailer;



    if (isset($_POST['simpan'])) {
        include 'config/koneksi.php';
        $simpan = mysqli_query($connect, "INSERT INTO tb_user (nama_user,email_user,username,password,status) VALUES ('$_POST[nama_perusahan]','$_POST[email_p]','$_POST[username]','$_POST[password_p]','user')");
        if (!$simpan) {
            echo '<div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <a href="register.php" class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
                </a>
                <strong>Proses Gagal!</strong> Kesalahan pada sistem, silahkan hubungi admin kami.
            </div>  
        </div>';
        } else {

            $name = 'www.stmikindonediapadang.com';
            $email = $_POST['email_p'];
            $subject = 'Kerjasama STMIK Indonesia Padang';
            $message = 'Akun ada sudah kami Verifikasi dengan Username: ' . $_POST['username'] . ' Password: ' . $_POST['password_p'];
            //Enable SMTP debugging. 
            $mail->SMTPDebug = 3;
            //Set PHPMailer to use SMTP.
            $mail->isSMTP();
            //Set SMTP host name                          
            $mail->Host = "tls://smtp.gmail.com"; //host mail server
            //Set this to true if SMTP host requires authentication to send email
            $mail->SMTPAuth = true;
            //Provide username and password     
            $mail->Username = "novrianuraini04@gmail.com";   //nama-email smtp          
            $mail->Password = "HENDRIA02";           //password email smtp
            //If SMTP requires TLS encryption then set it
            $mail->SMTPSecure = "tls";
            //Set TCP port to connect to 
            $mail->Port = 587;

            $mail->From = "novrianuraini04@gmail.com"; //email pengirim
            $mail->FromName = "stmikindonediapadang"; //nama pengirim

            $mail->addAddress($email); //email penerima

            $mail->isHTML(true);

            $mail->Subject = $subject; //subject
            $mail->Body    = $message; //isi email
            $mail->AltBody = "PHP mailer"; //body email (optional)

            // if (!$mail->send()) {
            //     echo "Mailer Error: " . $mail->ErrorInfo;
            // } else {
            //     echo "Message has been sent successfully";
            // }


            // batas email


            echo '<div class="col-md-12">
                <div class="alert alert-success" role="alert">
                <a href="register.php" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>                    
                    </a>
                    <strong>Sukses!</strong> Anda sudah terdaftar silahkan cek email anda, dan login untuk pengajuan permohonan kerjasama <a href="index.php" class="btn btn-danger btn-sm">Login</a>
                </div>
            </div>';
        }
    }

    ?>

    <div class="login-container lightmode" style="background: #00FF7F;">

        <div class="login-box animated fadeInDown">
            <div class="login-logo"></div>
            <div class="login-body" style="background: #1b1e24;">
                <div class="login-title"><strong>Buat Akun!!</div>
                <form action="" class="form-horizontal" method="POST">
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="text" class="form-control" style="background: aliceblue;" name="nama_perusahan" placeholder="Nama Perusahaan/ Perorangan" required="required" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="text" class="form-control" style="background: aliceblue;" name="username" placeholder="Username" required="required" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="text" class="form-control" style="background: aliceblue;" name="email_p" placeholder="E-mail" required="required" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="password" class="form-control" style="background: aliceblue;" name="password_p" placeholder="Password" required="required" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="password" class="form-control" style="background: aliceblue;" name="ulang_pasword" placeholder="Ulangi Password" required="required" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <a href="../index.php" class="btn btn-link btn-block">Back to website!</a>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-info btn-block" type="submit" name="simpan">Daftar Sekarang!</button>
                        </div>
                    </div>
                    <div class="login-subtitle">
                        Sudah Memiliki Akun? <a href="index.php">
                            <font color="#00FF7F">Log In</font>
                        </a>
                    </div>
                </form>
            </div>
        </div>

    </div>

</body>

</html>